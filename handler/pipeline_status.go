package handler

import (
    "context"
    "fmt"
    "net/http"
    "net/url"
    "sort"
    "strconv"
    "strings"
    "time"

    "github.com/gorilla/mux"
    "github.com/hasura/go-graphql-client"
    "github.com/rs/zerolog"
    "github.com/xanzy/go-gitlab"
    "golang.org/x/oauth2"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)


func GetPipelineResultHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(pipelineStatusHandler{glserver, db, config})
}

// static dict lookup tables and helper functions
func status_favicon(status string) string {
    switch strings.ToLower(status) {
        case util.GlStatusSuccess: return "check-circle-green.svg"
        case util.GlStatusFailed: return "x-circle-red.svg"
        case util.GlStatusRunning: return "hourglass-split-blue.svg"
        case util.GlStatusPending: return "pause-circle-yellow.svg"
        case util.GlStatusCanceled: return "slash-circle-black.svg"
        case util.GlStatusCreated: return "patch-question-black.svg"
        case util.GlStatusSkipped: return "patch-question-black.svg"
        default: return "patch-question-black.svg"
    }
}

func class_for_status(s string) string{
    switch strings.ToLower(s) {
        case util.GlStatusSuccess:  return "text-success"
        case util.GlStatusPending:  return "text-warning"
        case util.GlStatusFailed:   return "text-danger"
        case util.GlStatusRunning:  return "text-primary"
        case util.GlStatusCanceled: return  "text-dark"
        case util.GlStatusManual:  return "text-info"
        default: return "text-secondary"
    }
}

func combine_status(current, other string) string {
    current = strings.ToLower(current)
    other = strings.ToLower(other)
    if current == util.GlStatusCanceled || other == util.GlStatusCanceled { return util.GlStatusCanceled }
    if current == util.GlStatusFailed || other == util.GlStatusFailed { return util.GlStatusFailed }
    if current == util.GlStatusPending || other == util.GlStatusPending { return util.GlStatusPending }
    if current == util.GlStatusSuccess && other == util.GlStatusSuccess { return util.GlStatusSuccess }
    return other
}

func to_local_time(rfctime string) string {
    t, err := time.Parse(time.RFC3339, rfctime)
    if err != nil {
        return ""
    }
    tz, err := time.LoadLocation("Local")
    if err != nil {
        return ""
    }
    return t.In(tz).Format("02 Jan 2006 15:04")
}

func icon_for_status(status string, retried bool) string {
    if retried {
        return "arrow-repeat"
    }
    switch strings.ToLower(status) {
        case util.GlStatusSuccess: return "check-circle"
        case util.GlStatusPending: return "pause-circle"
        case util.GlStatusFailed: return "x-circle"
        case util.GlStatusRunning: return "hourglass-split"
        case util.GlStatusCanceled: return "slash-circle"
        case util.GlStatusManual: return "play-circle"
        default: return "patch-question"
    }
}

type pipelineStatusHandler struct {
    Glserver *gitlab.Client
    DB util.DB
    Config util.Config
}
func (h pipelineStatusHandler) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    vars := mux.Vars(r)

    if uniq_error, key_found := vars["uniq_error"]; key_found {
        logger.Debug().Msgf("Requesting unique error page %v", uniq_error)
        http.ServeFile(w, r, fmt.Sprintf("%v/%v", h.Config.ErrorsDir, uniq_error))
        return
    }

    gitlab_project_id := vars["gitlab_project_id"]
    gitlab_pipeline_id, err := strconv.Atoi(vars["gitlab_pipeline_id"])
    pie(logger.Error, err,"Invalid URL", http.StatusBadRequest)
    pipeline_id, err := strconv.ParseInt(vars["pipeline_id"], 10, 64)
    pie(logger.Error, err, "Invalid URL", http.StatusBadRequest)

    m := h.DB.GetMirrorUrl(pipeline_id)
    if r.URL.Query().Get("type") == "gitlab" {
        redirect_url := fmt.Sprintf("%v/-/pipelines/%v", m, gitlab_pipeline_id)
        http.Redirect(w, r, redirect_url, http.StatusFound)
        return
    }


    repository_id := h.DB.GetRepositoryId(pipeline_id)
    repo_url := h.DB.GetRepoUrl(repository_id)
    repo_type := util.GetRepoType(repo_url)

    var authorized bool
    var claims []string
    access_level := util.NoAccess
    if authorized, claims = is_authorized(r, w, false); authorized {
        access_level = h.DB.GetAccessLevelForRepoWithClaims(repository_id, claims)
    }

    gl_pipeline_iid, err := strconv.Atoi(r.URL.Query().Get("iid"))
    if err != nil {
        pipeline, resp, err := h.Glserver.Pipelines.GetPipeline(gitlab_project_id, gitlab_pipeline_id)
        pie(logger.Error, err, "Error getting pipeline result", resp.StatusCode)
        gl_pipeline_iid = pipeline.IID
    }

    token := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: h.Config.Gitlab.Token})
    httpClient := oauth2.NewClient(context.Background(), token)
    client := graphql.NewClient(h.Config.Gitlab.Url + "/api/graphql", httpClient).WithDebug(true)
    variables := map[string]interface{} {
        "project_path": graphql.ID(fmt.Sprintf("%v/%v/%v", h.Config.Gitlab.MirrorsPath, repository_id, pipeline_id)),
        "pipeline_iid": graphql.ID(fmt.Sprintf("%v", gl_pipeline_iid)),
    }

    fetch_pipeline := func(build_jobs_cursor, bridge_jobs_cursor, stages_cursor string) PipelineQuery {
        variables["build_jobs_cursor"] = build_jobs_cursor
        variables["bridge_jobs_cursor"] = bridge_jobs_cursor
        variables["stages_cursor"] = stages_cursor
        var query PipelineQuery
        // nosemgrep: gosec.G202-1
        err = client.Query(context.Background(), &query, variables)
        pie(logger.Error, err, "Failed executing GraphQl query to fetch the pipeline status", http.StatusBadRequest)
        return query
    }

    query := fetch_pipeline("", "", "")
    ppl := &query.Project.Pipeline
    for ; ppl.BuildJobs.PageInfo.HasNextPage ||
          ppl.BridgeJobs.PageInfo.HasNextPage ||
          ppl.Stages.PageInfo.HasNextPage ;
    {
              nextQuery := fetch_pipeline(ppl.BuildJobs.PageInfo.EndCursor, ppl.BridgeJobs.PageInfo.EndCursor, ppl.Stages.PageInfo.EndCursor)
              ppl.BuildJobs.Nodes = append(ppl.BuildJobs.Nodes, nextQuery.Project.Pipeline.BuildJobs.Nodes...)
              ppl.BuildJobs.PageInfo = nextQuery.Project.Pipeline.BuildJobs.PageInfo
              ppl.BridgeJobs.Nodes = append(ppl.BridgeJobs.Nodes, nextQuery.Project.Pipeline.BridgeJobs.Nodes...)
              ppl.BridgeJobs.PageInfo = nextQuery.Project.Pipeline.BridgeJobs.PageInfo
              ppl.Stages.Nodes = append(ppl.Stages.Nodes, nextQuery.Project.Pipeline.Stages.Nodes...)
              ppl.Stages.PageInfo = nextQuery.Project.Pipeline.Stages.PageInfo
    }

    var pr_id string
    ref := query.Project.Pipeline.Ref
    if strings.HasPrefix(ref, "__CSCSCI__pr") {
        pr_id = strings.Replace(ref, "__CSCSCI__pr", "", 1)
        ref = fmt.Sprintf("PR %v", pr_id)
    }
    var pr_url string
    if len(pr_id) > 0 {
        switch repo_type {
        case util.Github:
            pr_url = fmt.Sprintf("%v/pull/%v", repo_url, pr_id)
        case util.Gitlab:
            pr_url = fmt.Sprintf("%v/-/merge_requests/%v", repo_url, pr_id)
        // TODO: bitbucket
        }
    } else {
        switch repo_type {
        case util.Github:
            pr_url = fmt.Sprintf("%v/tree/%v", repo_url, ref)
        case util.Gitlab:
            pr_url = fmt.Sprintf("%v/-/tree/%v", repo_url, ref)
        }
    }

    var commit_url string
    switch repo_type {
    case util.Github:
        if len(pr_id) > 0 {
            commit_url = fmt.Sprintf("%v/pull/%v/commits/%v", repo_url, pr_id, query.Project.Pipeline.Sha)
        } else {
            commit_url = fmt.Sprintf("%v/commit/%v", repo_url, query.Project.Pipeline.Sha)
        }
    case util.Gitlab:
        if len(pr_id) > 0 {
            commit_url = fmt.Sprintf("%v/-/merge_requests/%v/diffs?commit_id=%v", repo_url, pr_id, query.Project.Pipeline.Sha)
        } else {
            commit_url = fmt.Sprintf("%v/-/commit/%v", repo_url, query.Project.Pipeline.Sha)
        }
    // case util.Github/util.Bitbucket TODO: bitbucket
    }

    login_url := ""
    if !authorized {
        login_url = util.BuildCIURL("/oauth2/auth?redirect=%v", &h.Config, url.QueryEscape(util.BuildCIURL(r.URL.String(), &h.Config)))
    }

    startedAt := to_local_time(query.Project.Pipeline.StartedAt)

    tmpl_data := statusTmplData{
        Ref: ref,
        Title: fmt.Sprintf("%v - %v", h.DB.GetRepositoryName(repository_id), h.DB.GetPipelineName(pipeline_id)),
        PrUrl: pr_url,
        CommitUrl: commit_url,
        Favicon: status_favicon(query.Project.Pipeline.Status),
        UrlPrefix: h.Config.URLPrefix,
        ProjectID: int64(gl_gid_to_id(query.Project.Id)),
        PipelineID: pipeline_id,
        GitlabPipelineID: gl_gid_to_id(query.Project.Pipeline.Id),
        GitlabPipelineIid: gl_pipeline_iid,
        AuthorizedAsManager: access_level >= util.ManagerAccess,
        LoginURL: login_url,
        CanCancel: query.Project.Pipeline.Cancelable,
        CanRestart: query.Project.Pipeline.Retryable,
        RestartCancelRedirect: fmt.Sprintf("/%v%v", h.Config.URLPrefix, r.URL),
        SHA: query.Project.Pipeline.Sha,
        StartedAt: startedAt,
        Pipelines: parse_pipeline(&query.Project.Pipeline.PipelineCommon, query.Project.Pipeline.BridgeJobs.Nodes, "", h.Config.URLPrefix, pipeline_id,gl_gid_to_id(query.Project.Id), logger),
    }

    err = util.ExecuteTemplate(w, "pipeline_status.html", &tmpl_data)
    pie(logger.Error, err, "Error rendering template pipeline_status.html", http.StatusInternalServerError)
}

func parse_pipeline(pipeline *PipelineCommon, bridge_jobs []BridgeJob, name string, urlprefix string, pipeline_id int64, gitlab_project_id int, logger *zerolog.Logger) []pipelineStatusTmplData {
    var ret []pipelineStatusTmplData
    var stages []stageTmplData
    ret = append(ret, pipelineStatusTmplData{
        Name: name,
        TextColor: class_for_status(pipeline.Status),
        Icon: icon_for_status(pipeline.Status, false),
        Url: fmt.Sprintf("/%v/pipeline/results/%v/%v/%v?iid=%v", urlprefix, pipeline_id, gitlab_project_id, gl_gid_to_id(pipeline.Id), pipeline.Iid),
        Partial: len(pipeline.BuildJobs.Nodes) >= 100,
    })
    jobsPerStage := map[string][]BuildJob{}
    for _, job := range pipeline.BuildJobs.Nodes {
        jobsPerStage[job.Stage.Name] = append(jobsPerStage[job.Stage.Name], job)
    }
    for _, jobs := range jobsPerStage {
        sort.Slice(jobs, func(i, j int) bool { return jobs[i].Name < jobs[j].Name })
    }
    for _, stage := range pipeline.Stages.Nodes {
        var jobs []jobTmplData
        for _, job := range jobsPerStage[stage.Name] {
            var duration string
            if job.Duration>0 {
                duration = fmt.Sprintf("%v sec", int(job.Duration))
            }
            artifactsHref := ""
            for _, artifact := range job.Artifacts.Nodes {
                if artifact.FileType == "ARCHIVE" {
                    artifactsHref = fmt.Sprintf("/%v/job/artifacts/download/%v/%v/%v/artifacts.zip", urlprefix, pipeline_id, gitlab_project_id, gl_gid_to_id(job.Id))
                }
            }
            startedAt := to_local_time(job.StartedAt)
            jobs = append(jobs, jobTmplData{
                Status: strings.ToLower(job.Status),
                Href: fmt.Sprintf("/%v/job/result/%v/%v/%v?iid=%v", urlprefix, pipeline_id, gitlab_project_id, gl_gid_to_id(job.Id), pipeline.Iid),
                TextColor: class_for_status(job.Status),
                Name: job.Name,
                Icon: icon_for_status(job.Status, job.Retried),
                Duration: duration,
                JobID: gl_gid_to_id(job.Id),
                CanRestart: job.Retryable,
                CanCancel: job.Cancelable,
                ArtifactsHref: artifactsHref,
                StartedAt: startedAt,
            })
        }
        if len(jobs) > 0 {
            stages = append(stages, stageTmplData{
                Name: stage.Name,
                TextColor: class_for_status(stage.Status),
                Icon: icon_for_status(stage.Status, false),
                Jobs: jobs,
            })
        }
    }
    for _, bridge_job := range bridge_jobs {
        ret = append(ret, parse_pipeline(bridge_job.DownstreamPipeline, nil, bridge_job.Name, urlprefix, pipeline_id,gitlab_project_id, logger)...)
    }
    ret[0].Stages = stages
    return ret
}


// convert a gitlab GID to an integer. gid have this format: gid://gitlab/Ci::Build/4596751540
func gl_gid_to_id(gid string) int {
    ret, _ := strconv.Atoi(gid[strings.LastIndex(gid, "/")+1:])
    return ret
}

type statusTmplData struct {
    Ref string
    Title string
    PrUrl string
    CommitUrl string
    Favicon string
    UrlPrefix string
    ProjectID int64
    PipelineID int64 // database pipeline_id
    GitlabPipelineID int
    GitlabPipelineIid int
    AuthorizedAsManager bool // user is logged in and access level >= ManagerAccess
    LoginURL string
    CanRestart bool
    CanCancel bool
    RestartCancelRedirect string
    SHA string
    StartedAt string
    Pipelines []pipelineStatusTmplData
}
type pipelineStatusTmplData struct {
    Name string
    TextColor string
    Icon string
    Url string
    Partial bool
    Stages []stageTmplData
}

type stageTmplData struct {
    Name string
    TextColor string
    Icon string
    Jobs []jobTmplData
}
type jobTmplData struct {
    Status string
    Href string
    TextColor string
    Name string
    Icon string
    Duration string
    StartedAt string
    JobID int
    Retried bool
    CanRestart bool
    CanCancel bool
    ArtifactsHref string
}

// GraphQL query
type PipelineQuery struct {
    Project Project `graphql:"project(fullPath: $project_path)"`
}
type Project struct {
    Name string
    Id string
    Pipeline PipelineWithBridgeJobs `graphql:"pipeline(iid: $pipeline_iid)"`
}
type PipelineCommon struct {
    Status string
    Id string
    Iid string
    Sha string `graphql:"sha(format: LONG)"`
    Ref string
    Cancelable bool
    Retryable bool
    StartedAt string
    Stages struct {
        Nodes []struct {
            Name string
            Status string
        }
        PageInfo PageInfo
    } `graphql:"stages(after: $stages_cursor)"`
    BuildJobs struct {
        Nodes []BuildJob
        PageInfo PageInfo
    } `graphql:"build_jobs: jobs(jobKind:BUILD, after: $build_jobs_cursor)"`
}
type PipelineWithBridgeJobs struct {
    PipelineCommon
    BridgeJobs struct {
        Nodes []BridgeJob
        PageInfo PageInfo
    } `graphql:"bridge_jobs: jobs(jobKind:BRIDGE, after: $bridge_jobs_cursor)"`
}
type BuildJob struct {
    Name string
    Id string
    Status string
    Duration int
    Cancelable bool
    Retryable bool
    Retried bool
    CanPlayJob bool
    AllowFailure bool
    FailureMessage string
    Stuck bool
    StartedAt string
    Stage struct {
        Name string
    }
    Artifacts struct { Nodes []Artifact }
}
type BridgeJob struct {
    Name string
    DownstreamPipeline *PipelineCommon
}
type Artifact struct {
    FileType string  // TRACE, DOTENV, ARCHIVE, METADATA
    Size string
}
type PageInfo struct {
    HasNextPage bool
    EndCursor string
}
