package handler

import (
    "fmt"
    "net/http"
    "os"
    "strconv"
    "strings"

    "github.com/IGLOU-EU/go-wildcard/v2"
    "github.com/go-git/go-git/v5"
    gitconfig "github.com/go-git/go-git/v5/config"
    "github.com/go-git/go-git/v5/plumbing"
    wh_bitbucket "github.com/go-playground/webhooks/v6/bitbucket"
    wh_github "github.com/go-playground/webhooks/v6/github"
    wh_gitlab "github.com/go-playground/webhooks/v6/gitlab"
    "github.com/rs/zerolog"
    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

var error_ci_run_no_pipeline_name = []byte("Error using comment cscs-ci run. You need to specify explicitly which pipelines you want to run (comma separated list of pipeline names).<br>Example comment: cscs-ci run pipeline_name1,pipeline_name2")
var error_ci_no_such_pipeline = []byte("Error using comment cscs-ci run. Pipeline could not be found")
var error_ci_untrusted_user = []byte("Error using comment cscs-ci run. The user is not allowed to trigger this pipeline")
var error_ci_mirror_creation_failure = []byte("Error creating mirror repository. Please contact CSCS for investigation")
var error_ci_mirror_push_failure = []byte("Error pushing changes to mirror repository. Please contact CSCS for investigation")


func GetWebhookCIHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(webhookCI{glserver, db, config})
}

type webhookCI struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
}

func (h webhookCI) Post(w http.ResponseWriter, r *http.Request) {
    // do some quick checks and see whether we need to go to background to do work
    logger := logging.GetReqLogger(r)
    repo_id, err := strconv.ParseInt(r.URL.Query().Get("id"), 10, 64)
    pie(logger.Error, err, "Failed parsing repo_id", http.StatusBadRequest)

    // disallow empty webhook secret - also if repo_id is invalid an empty string is returned
    webhook_secret := h.db.GetWebhookSecret(repo_id)
    if webhook_secret == "" {
        pie(logger.Error, fmt.Errorf("Unknown repo_id %v", repo_id), "Unknown id", http.StatusBadRequest)
    }
    repo_url := h.db.GetRepoUrl(repo_id)

    is_github := r.Header.Get("X-Github-Event") != ""
    is_gitlab := r.Header.Get("X-Gitlab-Event") != ""
    is_bitbucket    := r.Header.Get("X-Event-Key") != ""
    if !is_github && !is_gitlab && !is_bitbucket {
        pie(logger.Error, fmt.Errorf("Unknown event"), "Unknown event", http.StatusBadRequest)
    }

    // parse the event to one of our internal event structure - event will be nil if no interesting event was found.
    var event interface{}
    var event_url string
    switch {
    case is_github:
        hook ,err := wh_github.New(wh_github.Options.Secret(webhook_secret))
        pie(logger.Error, err, "Failed creating hook parsers", http.StatusInternalServerError)
        payload, err := hook.Parse(r, wh_github.PushEvent, wh_github.PullRequestEvent, wh_github.IssueCommentEvent)
        if err != nil && err != wh_github.ErrEventNotFound {
            pie(logger.Error, err, "Failed parsing webhook payload", http.StatusBadRequest)
        }
        switch payload.(type) {
        case wh_github.PushPayload:
            ev := payload.(wh_github.PushPayload)
            event = make_push_event(strings.Replace(ev.Ref, "refs/heads/", "", 1))
            event_url = ev.Repository.HTMLURL
        case wh_github.PullRequestPayload:
            ev := payload.(wh_github.PullRequestPayload)
            if ev.Action == "opened" || ev.Action == "synchronize" { // should we also include action=="edited"? This covers the case when the target branch could be changed
                event = make_pr_event(fmt.Sprintf("refs/pull/%v/head", ev.PullRequest.Number), ev.PullRequest.Base.Ref, ev.PullRequest.Head.Repo.Owner.Login, ev.PullRequest.Number, ev.PullRequest.Head.Repo.ID == ev.Repository.ID)
            }
            event_url = ev.Repository.HTMLURL
        case wh_github.IssueCommentPayload:
            ev := payload.(wh_github.IssueCommentPayload)
            if (ev.Action == "created" || ev.Action == "edited") && ev.Issue.PullRequest != nil && ev.Issue.PullRequest.URL != "" {
                event = make_comment_event(fmt.Sprintf("refs/pull/%v/head", ev.Issue.Number), ev.Comment.User.Login, ev.Comment.Body, ev.Issue.Number)
            }
            event_url = ev.Repository.HTMLURL
        }
    case is_gitlab:
        hook, err := wh_gitlab.New(wh_gitlab.Options.Secret(webhook_secret))
        pie(logger.Error, err, "Failed creating hook parsers", http.StatusInternalServerError)
        payload, err := hook.Parse(r, wh_gitlab.PushEvents, wh_gitlab.MergeRequestEvents, wh_gitlab.CommentEvents)
        if err != nil && err != wh_gitlab.ErrEventNotFound {
            pie(logger.Error, err, "Failed parsing webhook payload", http.StatusBadRequest)
        }
        switch payload.(type) {
        case wh_gitlab.PushEventPayload:
            ev := payload.(wh_gitlab.PushEventPayload)
            event = make_push_event(strings.Replace(ev.Ref, "refs/heads/", "", 1))
            event_url = ev.Project.WebURL
        case wh_gitlab.MergeRequestEventPayload:
            ev := payload.(wh_gitlab.MergeRequestEventPayload)
            if ev.ObjectAttributes.Action == "open" || ev.ObjectAttributes.Action == "reopen" || ev.ObjectAttributes.Action == "update" {
                event = make_pr_event(fmt.Sprintf("refs/merge-requests/%v/head", ev.ObjectAttributes.IID), ev.ObjectAttributes.TargetBranch, ev.User.UserName, ev.ObjectAttributes.IID, ev.ObjectAttributes.SourceProjectID == ev.ObjectAttributes.TargetProjectID)
            }
            event_url = ev.Project.WebURL
        case wh_gitlab.CommentEventPayload:
            ev := payload.(wh_gitlab.CommentEventPayload)
            // mr comment
            if ev.MergeRequest.IID > 0 {
                event = make_comment_event(fmt.Sprintf("refs/merge-requests/%v/head", ev.MergeRequest.IID), ev.User.UserName, ev.ObjectAttributes.Note, ev.MergeRequest.IID)
            }
            event_url = ev.Project.WebURL
        }
    case is_bitbucket:
        if r.URL.Query().Get("secret") != webhook_secret {
            pie(logger.Error, fmt.Errorf("secret=%v does not match the secret in database", r.URL.Query().Get("secret")), "Webhook secret mismatch", http.StatusBadRequest)
        }
        hook, err := wh_bitbucket.New()
        pie(logger.Error, err, "Failed creating hook parsers", http.StatusInternalServerError)
        payload, err := hook.Parse(r, wh_bitbucket.RepoPushEvent, wh_bitbucket.PullRequestCreatedEvent, wh_bitbucket.PullRequestUpdatedEvent, wh_bitbucket.PullRequestCommentCreatedEvent)
        if err != nil && err != wh_bitbucket.ErrEventNotFound {
            pie(logger.Error, err, "Failed parsing webhook payload", http.StatusBadRequest)
        }
        switch payload.(type) {
        case wh_bitbucket.RepoPushPayload:
            ev := payload.(wh_bitbucket.RepoPushPayload)
            var push_branches []string
            for _, change := range(ev.Push.Changes) {
                if change.New.Type == "branch" {
                    push_branches = append(push_branches, change.New.Name)
                }
            }
            event = make_push_event(push_branches...)
            event_url = ev.Repository.Links.HTML.Href
        case wh_bitbucket.PullRequestCreatedPayload:
            ev := payload.(wh_bitbucket.PullRequestCreatedPayload)
            // We cannot use Source.Commit.Hash, because it is the short hash and a git fetch would fail with the short hash. Therefore we use Source.Branch.Name, which
            // is not optimal because in theory we have a race condition, where a second commit could happen, before we fetch, and we would fetch a different hash
            //eventTmp := make_pr_event(ev.PullRequest.Source.Commit.Hash, ev.PullRequest.Destination.Branch.Name, ev.PullRequest.Author.NickName, ev.PullRequest.ID, ev.PullRequest.Source.Repository.UUID == ev.PullRequest.Destination.Repository.UUID)
            eventTmp := make_pr_event("refs/heads/"+ev.PullRequest.Source.Branch.Name, ev.PullRequest.Destination.Branch.Name, ev.PullRequest.Author.NickName, ev.PullRequest.ID, ev.PullRequest.Source.Repository.UUID == ev.PullRequest.Destination.Repository.UUID)
            // bitbucket is special, because it does not include the commits in the target repository, and the source code must be fetched
            // this can be relatively tricky for private repositories, because we clone with an SSH key (and it is untested)
            eventTmp.mustFetchFromRepo = bitbucket_fetch_from_remote(&ev.PullRequest)
            event = eventTmp
            event_url = ev.Repository.Links.HTML.Href
        case wh_bitbucket.PullRequestUpdatedPayload:
            ev := payload.(wh_bitbucket.PullRequestUpdatedPayload)
            eventTmp := make_pr_event("refs/heads/"+ev.PullRequest.Source.Branch.Name, ev.PullRequest.Destination.Branch.Name, ev.PullRequest.Author.NickName, ev.PullRequest.ID, ev.PullRequest.Source.Repository.UUID == ev.PullRequest.Destination.Repository.UUID)
            eventTmp.mustFetchFromRepo = bitbucket_fetch_from_remote(&ev.PullRequest)
            event = eventTmp
            event_url = ev.Repository.Links.HTML.Href
        case wh_bitbucket.PullRequestCommentCreatedPayload:
            ev := payload.(wh_bitbucket.PullRequestCommentCreatedPayload)
            eventTmp := make_comment_event("refs/heads/"+ev.PullRequest.Source.Branch.Name, ev.Actor.NickName, ev.Comment.Content.Raw, ev.PullRequest.ID)
            eventTmp.mustFetchFromRepo = bitbucket_fetch_from_remote(&ev.PullRequest)
            event= eventTmp
            event_url = ev.Repository.Links.HTML.Href
        }
    }

    if event == nil {
        logger.Info().Msgf("event is not processed any further")
        _, _ = w.Write([]byte("ok"))
    } else {
        // ensure that the registered url matches the event url - exit with an error if this event is coming from a different repo than the registered one
        pie(logger.Error, ensure_repo_allowed(repo_url, event_url), "Registered URL does not match the event URL", http.StatusBadRequest)

        // nosemgrep: gosec.G304-1
        reqLogFile, err := os.OpenFile(h.config.GetRequestLogPath(logging.GetReqCorrelationID(r)), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
        pie(logger.Error, err, "Failed opening requests log file for writing", http.StatusInternalServerError)
        logger, reqId := logging.LogReqHandlingToFile(r, reqLogFile)
        logger.Debug().Msg("Started request logging")
        // continue in background - we have some work to do
        go func() {
            defer func() {
                logger.Info().Msg("Finished processing webhook event")
                if err := reqLogFile.Close(); err != nil {
                    logging.Error(err, "Failed closing reqLogFile")
                }
            }()

            switch event.(type) {
            case pushBranchEvent:
                h.handle_push_event(event.(pushBranchEvent), repo_id, logger, reqLogFile)
            case prUpdateEvent:
                h.handle_pr_event(event.(prUpdateEvent), repo_id, logger, reqLogFile)
            case commentEvent:
                h.handle_comment_event(event.(commentEvent), repo_id, logger, reqLogFile)
            }
        }()
        _,_ = w.Write([]byte(strings.Replace(request_log_html, "REPLACE_URL", util.BuildCIURL("/webhook_log?reqId=%v", &h.config, reqId), 1)))
    }
}



func (h *webhookCI) handle_push_event(event pushBranchEvent, repo_id int64, logger *zerolog.Logger, reqLogFile *os.File) {
    event.repo_id = repo_id
    event.logger = logger
    event.handler = h

    repo_path := clone_path(&event.commonEvent)
    util.LockRepoDir(repo_path, logger)
    defer util.UnlockRepoDir(repo_path, logger)
    repository, err := clone_or_open(repo_path, &event.commonEvent, reqLogFile)
    if err != nil {
        logger.Error().Err(err).Msgf("Failed cloning repository error=%v", err)
        return
    }
    if err := fetch(repository, "", []gitconfig.RefSpec{gitconfig.RefSpec("+refs/heads/*:refs/heads/*")}, &event.commonEvent, reqLogFile); err != nil {
        logger.Error().Err(err).Msgf("Failed fetching updates from remote. Error=%v", err)
        return
    }
    var wait_channels []chan zeroT
    for _, pipeline_id := range h.db.GetPipelines(repo_id) {
        if err := ensure_mirror_repo(&event.commonEvent, pipeline_id); err != nil {
            logger.Error().Err(err).Msgf("Failed creating mirror repository error=%v", err)
            continue
        }
        branches_to_mirror := get_branches_to_mirror(&event.commonEvent, pipeline_id)
        var branches_to_push []string
        for _, event_branch := range event.branches {
            for _, branch_wildcard := range branches_to_mirror {
                if wildcard.Match(branch_wildcard, event_branch) {
                    branches_to_push = append(branches_to_push, event_branch)
                }
            }
        }
        if len(branches_to_push) == 0 {
            logger.Info().Msgf("Pipeline %v will mirror the branches %v, but this event refers to the branches %v, i.e. there is no intersection - nothing to do", h.db.GetPipelineName(pipeline_id), branches_to_mirror, event.branches)
            continue
        }
        // push all changes - single operation
        var refspecs []gitconfig.RefSpec
        for _, this_branch := range branches_to_push {
            refspecs = append(refspecs, gitconfig.RefSpec(fmt.Sprintf("+refs/heads/%v:refs/heads/%v", this_branch, this_branch)))
        }
        if err := push(repository, refspecs, pipeline_id, &event.commonEvent, reqLogFile); err != nil {
            logger.Error().Err(err).Msgf("Failed pushing branches to mirror repository. error=%v", err)
            continue
        }
        // trigger every branch in background, errors will be propagated through notifications
        for _, this_branch := range branches_to_push {
            if hash, err := repository.ResolveRevision(plumbing.Revision(this_branch)); err != nil {
                logger.Error().Err(err).Msgf("Failed getting hash for revision=%v", this_branch)
            } else {
                ch := make(chan zeroT)
                wait_channels = append(wait_channels, ch)
                go trigger_branch(&event.commonEvent, pipeline_id, this_branch, hash.String(), nil, ch)
            }
        }
    }
    // wait for all trigger background functions
    for _, ch := range wait_channels {
        <- ch
    }
}


func (h *webhookCI) handle_pr_event(event prUpdateEvent, repo_id int64, logger *zerolog.Logger, reqLogFile *os.File) {
    event.repo_id = repo_id
    event.logger = logger
    event.handler = h

    repo_path := clone_path(&event.commonEvent)
    util.LockRepoDir(repo_path, logger)
    defer util.UnlockRepoDir(repo_path, logger)
    repository, err := clone_or_open(repo_path, &event.commonEvent, reqLogFile)
    if err != nil {
        logger.Error().Err(err).Msgf("Failed cloning repository error=%v", err)
        return
    }
    pr_branch_name := fmt.Sprintf("refs/heads/__CSCSCI__pr%v", event.prNumber)
    if err := fetch(repository, event.mustFetchFromRepo, []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("%v:%v", event.srcRefspec, pr_branch_name))}, &event.commonEvent, reqLogFile); err != nil {
        logger.Error().Err(err).Msgf("Failed fetching PR changes from %v. Error=%v", event.mustFetchFromRepo, err)
        return
    }

    // trigger all pipelines where the user is a trusted user and the target branch is a CI enabled branch - both can vary for different pipelines
    var wait_channels []chan zeroT
    for _, pipeline_id := range h.db.GetPipelines(repo_id) {
        if h.db.GetTriggerPR(pipeline_id) == false {
            logger.Info().Msgf("The pipeline %v is not triggered automatically because trigger_pr is set to false", h.db.GetPipelineName(pipeline_id))
            continue // continue with next pipeline, trigger_pr can be different for different pipelines
        }
        if event.prInRepo == false {
            if util.SliceContains(get_git_users(&event.commonEvent, pipeline_id), event.user) == false {
                logger.Info().Msgf("This PR belongs to user %v, but this user is not a trusted user for the pipeline %v", event.user, h.db.GetPipelineName(pipeline_id))
                continue // continue with next pipeline, trusted users can be different for different pipelines
            }
        }
        targets_ci_enabled_branch := false
        for _, wildcard_branch := range get_branches_to_mirror(&event.commonEvent, pipeline_id) {
            if wildcard.Match(wildcard_branch, event.toBranch) {
                targets_ci_enabled_branch = true
            }
        }
        if targets_ci_enabled_branch == false {
            logger.Info().Msgf("This PR targets branch %v, but this branch is not a CI enabled branch. No pipeline will be triggered for pipeline %v", event.toBranch, h.db.GetPipelineName(pipeline_id))
            continue // continue with next pipeline, CI enabled branches can be different for different pipelines
        }
        if err := ensure_mirror_repo(&event.commonEvent, pipeline_id); err != nil {
            logger.Error().Err(err).Msgf("Failed creating mirror repository error=%v", err)
            return
        }
        if err := push(repository, []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("+%v:%v", pr_branch_name, pr_branch_name))}, pipeline_id, &event.commonEvent, reqLogFile); err != nil {
            logger.Error().Err(err).Msgf("Failed pushing branches to mirror repository. error=%v", err)
            continue
        }
        if hash, err := repository.ResolveRevision(plumbing.Revision(pr_branch_name)); err != nil {
            logger.Error().Err(err).Msgf("Failed getting hash for revision=%v", pr_branch_name)
        } else {
            ch := make(chan zeroT)
            wait_channels = append(wait_channels, ch)
            go trigger_branch(&event.commonEvent, pipeline_id, pr_branch_name, hash.String(), nil, ch)
        }
    }

    // wait for all trigger background functions
    for _, ch := range wait_channels {
        <- ch
    }
}


func (h *webhookCI) handle_comment_event(event commentEvent, repo_id int64, logger *zerolog.Logger, reqLogFile *os.File) {
    event.repo_id = repo_id
    event.logger = logger
    event.handler = h

    repo_path := clone_path(&event.commonEvent)
    util.LockRepoDir(repo_path, logger)
    defer util.UnlockRepoDir(repo_path, logger)

    repository, err := clone_or_open(repo_path, &event.commonEvent, reqLogFile)
    if err != nil {
        logger.Error().Err(err).Msgf("Failed fetching repository error=%v", err)
        return
    }
    pr_branch_name_simple := fmt.Sprintf("__CSCSCI__pr%v", event.prNumber)
    pr_branch_name := fmt.Sprintf("refs/heads/%v", pr_branch_name_simple)
    if err := fetch(repository, event.mustFetchFromRepo, []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("%v:%v", event.srcRefspec, pr_branch_name))}, &event.commonEvent, reqLogFile); err != nil {
        logger.Error().Err(err).Msgf("Failed fetching PR changes from %v. Error=%v", event.mustFetchFromRepo, err)
        return
    }
    hash, err := repository.ResolveRevision(plumbing.Revision(pr_branch_name));
    if err != nil {
        logger.Error().Err(err).Msgf("Failed getting hash for revision=%v", pr_branch_name)
        return
    }

    // check comment if we have to do further work
    commentFirstLine, _, _ := strings.Cut(event.comment, "\n")
    if strings.HasPrefix(commentFirstLine, "cscs-ci run") {
        if commentFirstLine == "cscs-ci run" && len(event.handler.db.GetPipelines(repo_id))>1 {
            notify_ci_error(&event.commonEvent, error_ci_run_no_pipeline_name, "Incorrect cscs-ci-run comment", pr_branch_name_simple, hash.String())
            return
        }
    }

    // trigger all pipelines where the user is a trusted user - it can vary for different pipelines
    var wait_channels []chan zeroT
    pipelines_to_trigger := event.parse_cscsci_run()
    //special case handling
    if len(pipelines_to_trigger)==1 && pipelines_to_trigger[0].pipeline_name == "" {
        pipelines_to_trigger[0].pipeline_name = h.db.GetPipelineName(h.db.GetPipelines(repo_id)[0])
    }
    for _, pipeline_to_trigger := range pipelines_to_trigger {
        pipeline_id := h.db.GetPipelineId(pipeline_to_trigger.pipeline_name, repo_id)
        if pipeline_id == -1 {
            logger.Error().Msgf("Pipeline with name=%v could not be found", pipeline_to_trigger.pipeline_name)
            notify_error(&event.commonEvent, fmt.Appendf(error_ci_no_such_pipeline, ". pipeline_name=%v", pipeline_to_trigger.pipeline_name), hash.String(), pipeline_to_trigger.pipeline_name, "Pipeline not found", pr_branch_name_simple)
            continue
        }
        commenting_user_is_trusted := false
        for _, wildcard_user := range get_git_users(&event.commonEvent, pipeline_id) {
            if wildcard.Match(wildcard_user, event.user) {
                commenting_user_is_trusted = true
            }
        }
        if commenting_user_is_trusted  == false{
            logger.Info().Msgf("The commenting user %v is not a trusted user for the pipeline %v", event.user, h.db.GetPipelineName(pipeline_id))
            notify_error(&event.commonEvent, fmt.Appendf(error_ci_untrusted_user, ". pipeline_name=%v", pipeline_to_trigger.pipeline_name), hash.String(), pipeline_to_trigger.pipeline_name, "Untrusted user comment", pr_branch_name_simple)
            continue // continue with next pipeline, trusted users can be different for different pipelines
        }
        if err := ensure_mirror_repo(&event.commonEvent, pipeline_id); err != nil {
            logger.Error().Err(err).Msgf("Failed creating mirror repository error=%v", err)
            notify_error(&event.commonEvent, fmt.Appendf(error_ci_mirror_creation_failure, ". pipeline_name=%v", pipeline_to_trigger.pipeline_name), hash.String(), pipeline_to_trigger.pipeline_name, "Mirror error", pr_branch_name_simple)
            continue
        }
        if err := push(repository, []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("+%v:%v", pr_branch_name, pr_branch_name))}, pipeline_id, &event.commonEvent, reqLogFile); err != nil {
            logger.Error().Err(err).Msgf("Failed pushing branches to mirror repository. error=%v", err)
            notify_error(&event.commonEvent, fmt.Appendf(error_ci_mirror_push_failure, ". pipeline_name=%v", pipeline_to_trigger.pipeline_name), hash.String(), pipeline_to_trigger.pipeline_name, "Mirror error", pr_branch_name_simple)
            continue
        }
        ch := make(chan zeroT)
        wait_channels = append(wait_channels, ch)
        go trigger_branch(&event.commonEvent, pipeline_id, pr_branch_name, hash.String(), pipeline_to_trigger.variables, ch)
    }

    // wait for all trigger background functions
    for _, ch := range wait_channels {
        <- ch
    }
}

func clone_path(ev *commonEvent) string {
    return fmt.Sprintf("/home/tmp/%v", ev.repo_id)
}

func clone_or_open(target_path string, ev *commonEvent, reqLogFile *os.File) (*git.Repository, error) {
    return util.GitCloneOrOpen(&ev.handler.db, &ev.handler.config, target_path, ev.repo_id, true, ev.logger, reqLogFile)
}

func fetch(repo *git.Repository, fromRepo string, refspecs []gitconfig.RefSpec, ev *commonEvent, reqLogFile *os.File) error {
    return util.GitFetch(&ev.handler.db, &ev.handler.config, repo, fromRepo, refspecs, ev.repo_id, ev.logger, reqLogFile)
}

func push(repo *git.Repository, refspecs []gitconfig.RefSpec, pipeline_id int64, ev *commonEvent, reqLogFile *os.File) error {
    return util.GitPush(&ev.handler.db, &ev.handler.config, repo, refspecs, ev.repo_id, pipeline_id, ev.logger, reqLogFile)
}

func ensure_mirror_repo(ev *commonEvent, pipeline_id int64) error {
    return util.EnsureMirrorRepo(&ev.handler.db, &ev.handler.config, ev.handler.glserver, ev.logger, ev.repo_id, pipeline_id)
}


func trigger_branch(ev *commonEvent, pipeline_id int64, branch, sha string, pipeline_vars varMap, ch chan zeroT) {
    defer func() {
        ch <- zeroT{}
    }()

    _, err := util.TriggerPipelineWithRetry(ev.handler.glserver, &ev.handler.config, &ev.handler.db, pipeline_id, branch, sha, pipeline_vars, ev.logger)
    if err != nil {
        ev.logger.Warn().Err(err).Msgf("Failed triggering pipeline %v, even after retrying. Error=%v", ev.handler.db.GetPipelineName(pipeline_id), err)
    }
}


func get_branches_to_mirror(ev *commonEvent, pipeline_id int64) []string {
    if pplBranches := ev.handler.db.GetBranchesPipeline(pipeline_id); len(pplBranches)==0 {
        return ev.handler.db.GetBranchesDefault(ev.repo_id)
    } else {
        return pplBranches
    }
}
func get_git_users(ev *commonEvent, pipeline_id int64) []string {
    if pplUsers := ev.handler.db.GetGitUsersPipeline(pipeline_id); len(pplUsers)==0 {
        return ev.handler.db.GetGitUsersDefault(ev.repo_id)
    } else {
        return pplUsers
    }
}


func ensure_repo_allowed(registered_url, event_url string) error {
    // we do NOT match case insensitive, i.e. it must be registered with the exact same case as it is being sent in the webhook payload
    // this is precautious - github.com / gitlab.com / bitbucket.org did NOT allow to register a project which only differs in the case spelling
    // but nevertheless we would open a door, if any of the providers changes their mind and changes the policy
    if registered_url != event_url {
        return fmt.Errorf("Registered url=%v does not match event url=%v", registered_url, event_url)
    }
    return nil
}



func bitbucket_fetch_from_remote(pr *wh_bitbucket.PullRequest) string {
    if pr.Source.Repository.UUID != pr.Destination.Repository.UUID {
        if pr.Source.Repository.IsPrivate {
            // private repositories are untested - it will probably fail due to missing rights to fetch from the foreign repository
            return fmt.Sprintf("bitbucket.org:%v", pr.Source.Repository.FullName)
        } else {
            return fmt.Sprintf("https://bitbucket.org/%v", pr.Source.Repository.FullName)
        }
    }
    return ""
}

func notify_error(ev *commonEvent, err_msg []byte, sha string, notification_name, desc, ref string) {
    util.NotifyError(err_msg, sha, notification_name, desc, ref, true, ev.repo_id, &ev.handler.config, &ev.handler.db, ev.logger)
}

func notify_ci_error(ev *commonEvent, error_msg []byte, desc, ref, sha string) {
    // notify first pipeline with status error - this can have unexpected behaviour, where the user wanted to write `cscs-ci run pipeline2`,
    // but forgot the term pipeline2, we would then send a notification to pipeline1 and afterwards the user starts correctly with the right comment
    // As a result there will be a failed notification on pipeline1 (with the error message, that the comment was wrong), and a real result for pipeline2
    all_pipelines := ev.handler.db.GetPipelines(ev.repo_id)
    if len(all_pipelines) == 0 {
        ev.logger.Error().Msgf("Could not send error notification because no pipelines are defined for repo_id=%v", ev.repo_id)
        return
    }
    notify_error(ev, error_msg, sha, ev.handler.db.GetPipelineName(all_pipelines[0]), desc, ref)
}


func (e *commentEvent) parse_cscsci_run() []triggerPipeline {
    // cscs-ci run pipeline_name_1;var1=val1;var2=val2,pipeline_name_2;varX=valY
    first_line, _, _ := strings.Cut(e.comment, "\n")
    if strings.HasPrefix(first_line, "cscs-ci run") == false {
        return nil
    }
    rest, _ := strings.CutPrefix(first_line, "cscs-ci run")
    if strings.TrimSpace(rest) == "" {
        return []triggerPipeline{{pipeline_name: "", variables: make(varMap)}}
    }
    pipeline_tokens := strings.Split(rest, ",")
    var ret []triggerPipeline
    for _, token := range pipeline_tokens {
        split_token := strings.Split(strings.TrimSpace(token), ";")
        trigger_pipeline := triggerPipeline{pipeline_name: split_token[0], variables: make(varMap)}
        for _, variable := range split_token[1:] {
            var_split := strings.SplitN(variable, "=", 2)
            if len(var_split) == 1 {
                trigger_pipeline.variables[var_split[0]] = ""
            } else {
                trigger_pipeline.variables[var_split[0]] = var_split[1]
            }
        }
        ret = append(ret, trigger_pipeline)
    }
    return ret
}

// event types - holds information to handle the event independent of the repository provider
type commonEvent struct {
    repo_id int64
    logger *zerolog.Logger
    handler *webhookCI
}
type pushBranchEvent struct {
    commonEvent
    branches []string
}
func make_push_event(branch... string) pushBranchEvent {
    return pushBranchEvent{
        branches: append([]string{}, branch...),
    }
}


type prUpdateEvent struct {
    commonEvent
    srcRefspec string
    toBranch string
    user string
    mustFetchFromRepo string // if not empty then srcRefspec must first be fetched from this repo, i.e. `git fetch $mustFetchFromRepo $srcRefSpec:some_name`
    prNumber int64
    prInRepo bool
}
func make_pr_event(srcRefspec, toBranch, user string, prNumber int64, prInRepo bool) prUpdateEvent {
    return prUpdateEvent {
        srcRefspec: srcRefspec,
        toBranch: toBranch,
        user: user,
        prNumber: prNumber,
        prInRepo: prInRepo,
    }
}

type commentEvent struct {
    commonEvent
    srcRefspec string
    mustFetchFromRepo string // see prUpdateEvent.mustFetchFromRepo
    user string
    comment string
    prNumber int64
}
func make_comment_event(srcRefspec, user, comment string, prNumber int64) commentEvent {
    return commentEvent {
        srcRefspec: srcRefspec,
        user: user,
        comment: comment,
        prNumber: prNumber,
    }
}

type zeroT struct{}
type varMap map[string]string
type triggerPipeline struct {
    pipeline_name string
    variables varMap
}
