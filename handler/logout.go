package handler

import (
    "fmt"
    "net/http"
    "net/url"
    
    "github.com/golang-jwt/jwt/v5"
    
    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

type logout struct{
    config *util.Config
    db *util.DB
}
func (h logout) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    session := GetSessionStore().GetSession(r, sessionAuth)
    if token, exists := session.Values[sessionAuthIdTokenKey]; exists {
        logger.Debug().Msg("Logout: Found an existing session")
        // a session exists with a token - enforce online check, to see if we have to forward to KC
        if authorized, _ := is_authorized(r, w, true); authorized {
            logger.Debug().Msgf("Logout: Session is still valid and I am authorized token=%#v", token)
            _ = GetSessionStore().Delete(r, w, session)
            redirect := fmt.Sprintf("%v?id_token_hint=%v&post_logout_redirect_uri=%v", h.config.OpenIdConfig.LogoutURL, token, url.QueryEscape(util.BuildCIURL("/logout", h.config)))
            //redirect := fmt.Sprintf("%v?id_token_hint=%v", h.config.OpenIdConfig.LogoutURL, token)
            http.Redirect(w, r, redirect, http.StatusFound)
        } else {
            // keycloak does not consider us logged in anymore, since authorized==false
            logger.Debug().Msg("Logout: We are not authorized anymore")
            if r.URL.Query().Get("sid") != "" {
                jw, _ := jwt.Parse(token.(string), jwks_keyfunc.Keyfunc)
                logger.Warn().Msgf("Requesting to logout for sid=%v, and jwt['sid']=%v", r.URL.Query().Get("sid"), jw.Claims.(jwt.MapClaims)["sid"])
            } else if r.URL.Query().Get("stop_redirect") != "true" {
                http.Redirect(w, r, util.BuildCIURL("/logout?stop_redirect=true", h.config), http.StatusFound)
            }
            _ = GetSessionStore().Delete(r, w, session)
        }
    } else {
        _, _ = w.Write([]byte("You are logged out"))
    }
}

// backchannel logout is not needed, since frontchannel logout is implemented
//func (h logout) Post(w http.ResponseWriter, r *http.Request) {
//    logger := logging.GetReqLogger(r)
//    r.ParseForm()
//    if token, err := jwt.Parse(r.FormValue("logout_token"), jwks_keyfunc.Keyfunc); err != nil {
//        logger.Error().Err(err).Msg("Error parsing logout_token")
//    } else {
//        h.db.DeleteWebsession(token.Claims.(jwt.MapClaims)["sid"].(string))
//    }
//}

func GetLogoutHandler(config *util.Config, db *util.DB) func(w http.ResponseWriter, r *http.Request) {
    return wrap(logout{config: config, db: db})
}
