package handler

import (
    "bytes"
    "encoding/json"
    "fmt"
    "io"
    "io/ioutil"
    "net/http"
    "os"
    "runtime"
    "strconv"
    "strings"

    wh_gitlab "github.com/go-playground/webhooks/v6/gitlab"
    "github.com/rs/zerolog"
    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

const request_log_html string = `The webhook event is being processed in the background.
Further details can be found at REPLACE_URL.
Please bear in mind that the background process can take several minutes and the log contents are incomplete for very recent events`

func GetWebhookGitlabPipelineHandler(glserver *gitlab.Client, db util.DB, config util.Config, redis *util.Redis) func(w http.ResponseWriter, r *http.Request) {
    return wrap(webhookGitlabPipeline{glserver, db, config, redis, nil})
}

type webhookGitlabPipeline struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
    redis *util.Redis
    body []byte
}

func (h webhookGitlabPipeline) Post(w http.ResponseWriter, r *http.Request) {
    // run everything in background, because webhooks must be quick and successful. Any further debug information will be saved in a separate logfile that can be
    // retrieved later
    // nosemgrep: gosec.G304-1
    reqLogFile, err := os.OpenFile(h.config.GetRequestLogPath(logging.GetReqCorrelationID(r)), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
    pie(logging.GetReqLogger(r).Error, err, "Failed opening requests log file for writing", http.StatusInternalServerError)
    logger, reqId := logging.LogReqHandlingToFile(r, reqLogFile)
    logger.Debug().Msg("Started request logging")
    go func() {
        defer func() {
            if panicVal := recover(); panicVal != nil {
                panicStr := fmt.Sprint(panicVal)
                stackbuf := make([]byte, 2048)
                n := runtime.Stack(stackbuf, false)
                stackbuf = stackbuf[:n]

                logger.Error().
                    Str("panic", panicStr).
                    Bytes("stack", stackbuf).
                    Str("id", reqId).
                    Msgf("%s %s failed with %v", r.Method, r.RequestURI, panicStr)
            }
            logger.Info().Msg("Finished processing webhook event")
            if err := reqLogFile.Close(); err != nil {
                logging.Error(err, "Failed closing reqLogFile")
            }
        }()
    //    repo_id, err := strconv.ParseInt(r.URL.Query().Get("repository_id"), 10, 64)
        pipeline_id, err := strconv.ParseInt(r.URL.Query().Get("pipeline_id"), 10, 64)
        hook, err := wh_gitlab.New(wh_gitlab.Options.Secret(h.db.GetMirrorWebhookSecret(pipeline_id)))
        pie(logger.Error, err, "Failed creating new webhook gitlab webook handler", http.StatusBadRequest)
        // there is some "bug" in the gitlab webhook, where a JobEvent is forwarded to a BuildEvent, therefore we need to catch the BuildEvent too
        // BuildEvent does not even exist in newer versions of gitlab. Also BuildEvent has fewer fields, but we do not need the additional
        // fields, so it is fine. I fixed the bug in my fork at github.com/finkandreas/webhooks, but we rely for consistency on the official
        // package. Also there is an open PR https://github.com/go-playground/webhooks/pull/146
        // Also I opened an issue about it: https://github.com/go-playground/webhooks/issues/175
        if h.config.ReportingConfig.Enabled {
            // we want to send the payload for reporting - store it in handler
            h.body, err = io.ReadAll(r.Body)
            pie(logger.Error, err, "Failed reading body from request", http.StatusInternalServerError)
            r.Body = ioutil.NopCloser(bytes.NewBuffer(h.body))
        }
        payload, err := hook.Parse(r, wh_gitlab.PipelineEvents, wh_gitlab.JobEvents, wh_gitlab.BuildEvents)
        if err != nil {
           if err != wh_gitlab.ErrEventNotFound {
               logger.Warn().Msgf("Received a gitlab webhook event that we did not expect. Header[X-Gitlab-Event]=%v", r.Header.Get("X-Gitlab-Event"))
           } else {
                pie(logger.Error, err, "Failed parsing webhook payload", http.StatusBadRequest)
            }
        }
        switch payload.(type) {
            case wh_gitlab.PipelineEventPayload:
                h.handlePipelineEvent(payload.(wh_gitlab.PipelineEventPayload), pipeline_id, logger)
            case wh_gitlab.JobEventPayload:
                h.handleJobEvent(payload.(wh_gitlab.JobEventPayload), pipeline_id, logger)
            case wh_gitlab.BuildEventPayload:
                h.handleBuildEvent(payload.(wh_gitlab.BuildEventPayload), pipeline_id, logger)
        }
    }()
    _,_ = w.Write([]byte(strings.Replace(request_log_html, "REPLACE_URL", util.BuildCIURL("/webhook_log?reqId=%v", &h.config, reqId), 1)))
}

func (h webhookGitlabPipeline) handlePipelineEvent(e wh_gitlab.PipelineEventPayload, pipeline_id int64, logger *zerolog.Logger) {
    repo_id := h.db.GetRepositoryId(pipeline_id)
    status := e.ObjectAttributes.Status
    if status == util.GlStatusFailed || status == util.GlStatusSuccess || status == util.GlStatusCanceled {
        h.doReporting(logger, 0, float64(e.ObjectAttributes.Duration), repo_id)
    }

    if h.db.GetMirrorUrl(pipeline_id) != e.Project.WebURL {
        pie(logger.Error, condition_error{"The event URL does not match the registered mirror URL"}, "", http.StatusBadRequest)
    }

    // special case where this is the container builder service - we do not send a notification in this case to the original repository
    if h.config.Gitlab.ContainerBuilderServicePipelineId == pipeline_id {
        logger.Debug().Msg("Not sending a notification since this is the container-builder-service")
        return
    }
    if h.config.Gitlab.UenvBuilderServicePipelineId == pipeline_id {
        logger.Debug().Msg("Not sending a notification since this is the uenv-builder-service")
        return
    }

    var cscs_notification_context string
    for _, pipeline_var := range e.ObjectAttributes.Variables {
        // override default notification context, even child pipelines can set this to allow a notification
        if pipeline_var.Key == "CSCS_NOTIFICATION_CONTEXT" {
            cscs_notification_context = pipeline_var.Value
            break
        }
    }
    // if source is a parent_pipeline we only send a notification when the notification context is explicitly set
    // if the status is != skipped we send a notification. When the status==skipped and source==api, then this is a pipeline with only manual jobs --> send a success notification
    if (e.ObjectAttributes.Source != "parent_pipeline" || cscs_notification_context != "") && (e.ObjectAttributes.Status != util.GlStatusSkipped || e.ObjectAttributes.Source == "api") {
        pipelinedata, _, err := h.glserver.Pipelines.GetPipeline(int(e.Project.ID), int(e.ObjectAttributes.ID))
        pie(logger.Error, err, "Failed getting pipeline data via gitlab REST api", http.StatusInternalServerError)
        commit_url := util.GetNotificationUrl(h.db.GetRepoUrl(repo_id), e.ObjectAttributes.SHA)
        status := e.ObjectAttributes.Status
        if status == util.GlStatusSkipped {
            status = util.GlStatusSuccess
        }
        target_url := util.BuildCIURL("/pipeline/results/%v/%v/%v?iid=%v", &h.config, pipeline_id, e.Project.ID, e.ObjectAttributes.ID, pipelinedata.IID) // e.ObjectAttributes.IID)
        if ! h.db.IsPrivateRepo(repo_id) {
            target_url = target_url + "&type=gitlab"
        }
        pipeline_name := h.db.GetPipelineName(pipeline_id)
        notification_token := h.db.GetNotificationToken(repo_id)

        if cscs_notification_context == "" {
            cscs_notification_context = pipeline_name
        }

        switch status {
        case util.GlStatusSuccess, util.GlStatusFailed, util.GlStatusSkipped, util.GlStatusCanceled:
            if err := h.db.DelStatusUpdateNeeded(pipeline_id, target_url, e.ObjectAttributes.SHA, pipeline_name); err != nil {
                logging.Error(err, "Failed deleting StatusUpdateNeeded from database")
            }
        default:
            if err := h.db.AddStatusUpdateNeeded(pipeline_id, target_url, e.ObjectAttributes.SHA, pipeline_name); err != nil {
                logging.Error(err, "Failed adding StatusUpdateNeeded to database")
            }
        }

        if notification_token == "" {
            logging.Infof("NotifyRepo at url=%v, but notification_token is empty. Not trying to notify", commit_url)
        } else {
            resp, err := util.NotifyRepo(commit_url, status, target_url, cscs_notification_context, notification_token, "", e.ObjectAttributes.Ref)
            pie(logger.Error, err, "Failed notifying original repo", http.StatusBadRequest)
            if resp != nil { // resp can be nil, even when err==nil. Do not treat as error
                if err := util.CheckResponse(resp); err != nil {
                    // most probably an incorrect notification token
                    logger.Warn().Err(err).Msgf("Failed notifiying original repository %v", h.db.GetRepositoryName(repo_id))
                }
            }
        }
    }
}


func (h webhookGitlabPipeline) handleJobBuildEvent(url string, build_duration float64, status string, job_id, pipeline_id int64, logger *zerolog.Logger) {
    repo_id := h.db.GetRepositoryId(pipeline_id)
    if status == util.GlStatusCanceled {
        _ = h.redis.SetJobCanceled(job_id, true)
    }
    if status == util.GlStatusFailed || status == util.GlStatusSuccess || status == util.GlStatusCanceled {
        h.doReporting(logger, job_id, build_duration, repo_id)
    }
    if !strings.HasPrefix(url, h.db.GetMirrorUrl(pipeline_id)) {
        pie(logger.Error, condition_error{"The event URL does not match the registered mirror URL"}, "", http.StatusBadRequest)
    }
    if status == util.GlStatusSuccess || status == util.GlStatusFailed || status == util.GlStatusCanceled {
        job_creds, err := h.db.GetCredentials(job_id)
        pie(logger.Error, err, "Failed getting all job tokens from database", http.StatusInternalServerError)
        for _, cred := range job_creds {
            util.GetTokenDeleter().DeleteToken(cred)
        }
    }
}
func (h webhookGitlabPipeline) handleBuildEvent(e wh_gitlab.BuildEventPayload, pipeline_id int64, logger *zerolog.Logger) {
    h.handleJobBuildEvent(e.Repository.GitHTTPURL, e.BuildDuration, e.BuildStatus, e.BuildID, pipeline_id, logger)
}
func (h webhookGitlabPipeline) handleJobEvent(e wh_gitlab.JobEventPayload, pipeline_id int64, logger *zerolog.Logger) {
    h.handleJobBuildEvent(e.Repository.GitHTTPURL, e.BuildDuration, e.BuildStatus, e.BuildID, pipeline_id, logger)
}

type Kafka struct {
    Records []KafkaRecord `json:"records"`
}
type KafkaRecord struct {
    Value map[string]any `json:"value"`
}
func (h *webhookGitlabPipeline) doReporting(logger *zerolog.Logger, job_id int64, duration float64, repo_id int64) {
    if h.config.ReportingConfig.Enabled == false {
        return
    }
    obj := make(map[string]any)
    if err := json.Unmarshal(h.body, &obj); err != nil {
        logger.Warn().Msgf("Failed decoding body as json object with err=%v. body=%v", err, string(h.body))
        return
    }
    obj["data_stream.type"] = h.config.ReportingConfig.Datastream.Type
    obj["data_stream.dataset"] = h.config.ReportingConfig.Datastream.Dataset
    obj["orchestrator"] = map[string]map[string]string{"cluster": {"name": h.config.ReportingConfig.Datastream.Namespace}}
    //obj["kubernetes.amespace"] = h.config.ReportingConfig.Datastream.Namespace

    if obj_attrs, ok := obj["object_attributes"].(map[string]any); ok {
        if vars, ok := obj_attrs["variables"].([]map[string]any); ok {
            for _, thisvar := range vars {
                thisvar["value"] = "--REDACTED--"
            }
        }
    }

    if job_id > 0 {
        if redis_job_data, err := h.redis.GetJobInfo(job_id); err != nil {
            logger.Error().Err(err).Msgf("Failing getting info from redis for jobid=%v", job_id)
        } else {
            var ok bool
            obj["slurm_job_id"] = redis_job_data["slurm_job_id"] // it is ok to be non-existent --> empty string
            if obj["allocation_wait_time"], ok = redis_job_data["allocation_wait_time"]; ok == false {
                logger.Info().Msgf("Could not find allocation_wait_time for job_id=%v in redis data. Setting it to 0", job_id)
                obj["allocation_wait_time"] = 0
            } else {
                obj["allocation_wait_time"], err = strconv.Atoi(redis_job_data["allocation_wait_time"])
                if err != nil {
                    logger.Error().Err(err).Msgf("Failed parsing allocation_wait_time from redis job data for jobid=%v", job_id)
                    obj["allocation_wait_time"] = 0
                }
            }
            clusterObj := map[string]string{}
            if clusterObj["name"], ok = redis_job_data["machine"]; ok == false {
                logger.Info().Msgf("Could not find machine for job_id=%v in redis data. Guessing from runner tags", job_id)
                if runner, ok := obj["runner"]; ok && runner != nil {
                    if tags, ok := runner.(map[string]any)["tags"]; ok {
                        clusterObj["name"] = strings.Split(tags.([]any)[0].(string), "-")[0]
                    }
                }
            }
            obj["cluster"] = clusterObj
            num_nodes := 1
            if num_nodes_str, ok := redis_job_data["num_nodes"]; ok {
                if num_nodes, err = strconv.Atoi(num_nodes_str); err != nil {
                    logger.Error().Err(err).Msgf("Failed parsing num_nodes as integer for job_id=%v", job_id)
                    num_nodes = 1
                }
            } else {
                logger.Info().Msgf("Could not find num_nodes for job_id=%v in redis data. Assuming it is 1", job_id)
            }
            obj["num_nodes"] = num_nodes
            obj["allocation_node_hours"] = (duration-float64(obj["allocation_wait_time"].(int)))*float64(num_nodes)/3600.0
            if obj["runner_mode"], ok = redis_job_data["mode"]; ok == false {
                logger.Info().Msgf("Could not find mode in redis job data for job_id=%v", job_id)
                obj["runner_mode"] = "unknown"
            }
            obj["ciext_repository_id"] = repo_id
        }
    }

    resp, err := util.DoJsonRequest("POST", h.config.ReportingConfig.URL, map[string]string{"Content-Type": "application/vnd.kafka.json.v2+json"}, Kafka{Records: []KafkaRecord{{Value: obj}}})
    if err != nil {
        logger.Warn().Msgf("Failed posting to %v with err=%v", h.config.ReportingConfig.URL, err)
        return
    }

    if err := util.CheckResponse(resp); err != nil {
        logger.Warn().Msgf("Request to kafka did not succeed. response=%+v", resp)
        return
    }
}
