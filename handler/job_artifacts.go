package handler

import (
    "io"
    "net/http"
    "strconv"


    "github.com/xanzy/go-gitlab"
    "github.com/gorilla/mux"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetJobArtifactsHandler(glserver *gitlab.Client, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(jobArtifactsHandler{glserver, config})
}

type jobArtifactsHandler struct {
    Glserver *gitlab.Client
    Config util.Config
}

func (h jobArtifactsHandler) Get(w http.ResponseWriter, r* http.Request) {
    w.Header().Add("Content-Type", "application/octet-stream")
    logger := logging.GetReqLogger(r)
    vars := mux.Vars(r)
    //pipeline_id := vars["pipeline_id"]
    gitlab_project_id := vars["gitlab_project_id"]
    job_id, err := strconv.Atoi(vars["job_id"])
    pie(logger.Error, err, "Invalid URL", http.StatusBadRequest)

    artifactReader, _, err := h.Glserver.Jobs.GetJobArtifacts(gitlab_project_id, job_id)
    pie(logger.Error, err, "Failed downloading artifacts", http.StatusBadRequest)

    _, err = io.Copy(w, artifactReader)
    pie(logger.Error, err, "Failed copying artifact to response", http.StatusBadRequest)
}
