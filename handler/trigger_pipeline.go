package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	gitconfig "github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v3"

	"cscs.ch/cicd-ext-mw/logging"
	"cscs.ch/cicd-ext-mw/util"
)

const statusKey = "status"
const pipelineIdKey = "pipeline_id"
const gitlabPipelineIdKey = "gitlab_pipeline_id"
const fullLogKey = "full_log_url"

func GetTriggerPipelineHandler(glserver *gitlab.Client, db util.DB, config util.Config, redis *util.Redis) func(w http.ResponseWriter, r *http.Request) {
    return wrap(triggerPipelineHandler{glserver: glserver, db: db, config: config, redis: redis})
}

type triggerPipelineHandler struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
    redis *util.Redis
}

type triggerPipelinePostBody struct {
    Ref  string `yaml:"ref"`
    Pipeline  string `yaml:"pipeline"`
    Variables map[string]string `yaml:"variables"`
}

func (h triggerPipelineHandler) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    repo_id := authorized_for_repo(r)
    if repo_id == -1 {
        w.Header().Set("WWW-Authenticate", `Basic realm="Trigger pipeline", charset="UTF-8"`)
        http.Error(w, "Need login to trigger pipeline", http.StatusUnauthorized)
        return
    }
    var trigger_descr triggerPipelinePostBody
    err := yaml.NewDecoder(r.Body).Decode(&trigger_descr)
    pie(logger.Error, err, fmt.Sprintf("The provided data in body could not be parsed. Error=%v", err), http.StatusBadRequest)

    pipeline_id := h.db.GetPipelineId(trigger_descr.Pipeline, repo_id)
    if pipeline_id == -1 {
        err = pipeline_not_found_error{trigger_descr.Pipeline}
        pie(logger.Warn, err, err.Error(), http.StatusBadRequest)
    }
    // nosemgrep: gosec.G304-1
    reqLogFile, err := os.OpenFile(h.config.GetRequestLogPath(logging.GetReqCorrelationID(r)), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
    pie(logger.Error, err, "Failed opening requests log file for writing", http.StatusInternalServerError)
    logger, reqId := logging.LogReqHandlingToFile(r, reqLogFile)
    logger.Debug().Msg("Started request logging")
    err = h.redis.StoreTriggerStatus(reqId, map[string]string{statusKey: "cloning", fullLogKey: util.BuildCIURL("/webhook_log?reqId=%v", &h.config, reqId)})
    pie(logger.Error, err, "Failed storing data in redis cache", http.StatusInternalServerError)

    // continue in background - we have some work to do
    go func() {
        defer func() {
            logger.Info().Msg("Finished processing webhook event")
            if err := reqLogFile.Close(); err != nil {
                logging.Error(err, "Failed closing reqLogFile")
            }
        }()

        target_path := fmt.Sprintf("/home/tmp/%v", repo_id)
        util.LockRepoDir(target_path, logger)
        defer util.UnlockRepoDir(target_path, logger)

        status := "failure"
        defer func() {
            err = h.redis.StoreTriggerStatus(reqId, map[string]string{statusKey: status})
            if err != nil {
                logger.Error().Err(err).Msgf("Failed storing status in redis cache. Error=%v", err)
                return
            }
        }()

        repo, err := util.GitCloneOrOpen(&h.db, &h.config, target_path, repo_id, true, logger, reqLogFile)
        if err != nil {
            logger.Error().Err(err).Msgf("Failed cloning repository error=%v", err)
            return
        }
        if err := util.GitFetch(&h.db, &h.config, repo, "", []gitconfig.RefSpec{gitconfig.RefSpec("+refs/heads/*:refs/heads/*")}, repo_id, logger, reqLogFile); err != nil {
            logger.Error().Err(err).Msgf("Failed fetching updates from remote. Error=%v", err)
            return
        }
        if err := h.redis.StoreTriggerStatus(reqId, map[string]string{statusKey: "mirroring"}); err != nil {
            logger.Error().Err(err).Msgf("Failed storing status in redis cache. Error=%v", err)
            return
        }
        if err := util.EnsureMirrorRepo(&h.db, &h.config, h.glserver, logger, repo_id, pipeline_id); err != nil {
            logger.Error().Err(err).Msgf("Failed creating mirror repository error=%v", err)
            return
        }
        refspecs := []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("+refs/heads/%v:refs/heads/%v", trigger_descr.Ref, trigger_descr.Ref))}
        if err := util.GitPush(&h.db, &h.config, repo, refspecs, repo_id, pipeline_id, logger, reqLogFile); err != nil {
            logger.Error().Err(err).Msgf("Failed pushing branches to mirror repository. error=%v", err)
            return
        }
       hash , err := repo.ResolveRevision(plumbing.Revision(trigger_descr.Ref))
        if err != nil {
            logger.Error().Err(err).Msgf("Failed getting SHA for reference %v", trigger_descr.Ref)
            return
        }
        if err := h.redis.StoreTriggerStatus(reqId, map[string]string{statusKey: "triggering"}); err != nil {
            logger.Error().Err(err).Msgf("Failed storing status in redis cache. Error=%v", err)
            return
        }
        if gitlab_pipeline_id, err := util.TriggerPipelineWithRetry(h.glserver, &h.config, &h.db, pipeline_id, trigger_descr.Ref, hash.String(), trigger_descr.Variables, logger); err != nil {
            logger.Warn().Err(err).Msgf("Failed triggering pipeline err=%v", err)
            return
        } else {
            status = "triggered_successfully"
            err = h.redis.StoreTriggerStatus(reqId, map[string]string{pipelineIdKey: fmt.Sprintf("%v", pipeline_id),
                                                                      gitlabPipelineIdKey: fmt.Sprintf("%v", gitlab_pipeline_id),
            })
            if err != nil {
                logger.Error().Err(err).Msgf("Failed storing status in redis cache. Error=%v", err)
                return
            }
        }
    }()
    _, _ = w.Write([]byte(strings.Replace(request_log_html, "REPLACE_URL", util.BuildCIURL("/webhook_log?reqId=%v", &h.config, reqId), 1)))
    _, _ = w.Write(fmt.Appendf(nil, "\nYou can also get the trigger status at %v", util.BuildCIURL("/pipeline/trigger?reqId=%v", &h.config, reqId)))
}


func (h triggerPipelineHandler) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    reqId := r.URL.Query().Get("reqId")
    if reqId == "" {
        pie(logger.Error, condition_error{`Missing query parameter "reqId"`}, "", http.StatusBadRequest)
    }

    redisData, err := h.redis.GetTriggerStatus(reqId)
    pie(logger.Error, err, "Failed getting pipeline status from cache", http.StatusBadRequest)

    if redisData[statusKey] == "triggered_successfully" {
        // enrich redisData with other information
        pipeline_id, err := strconv.ParseInt(redisData[pipelineIdKey], 10, 64)
        pie(logger.Error, err, "Failed parsing pipeline_id as int64", http.StatusInternalServerError)
        repo_id := h.db.GetRepositoryId(pipeline_id)
        gitlab_pipeline_id, err := strconv.Atoi(redisData[gitlabPipelineIdKey])
        pie(logger.Error, err, "Failed parsing gitlab_pipeline_id as int", http.StatusInternalServerError)

        pipeline, _, err := h.glserver.Pipelines.GetPipeline(fmt.Sprintf("%v/%v/%v", h.config.Gitlab.MirrorsPath, repo_id, pipeline_id), gitlab_pipeline_id, util.GitlabRequestMustSucceed(context.Background()))
        if err != nil {
            logger.Error().Err(err).Msgf("Failed fetching pipeline data. err=%v", err)
        }
        target_url := util.BuildCIURL("/pipeline/results/%v/%v/%v?iid=%v", &h.config, pipeline_id, pipeline.ProjectID, pipeline.ID, pipeline.IID)
        if ! h.db.IsPrivateRepo(repo_id) {
            target_url = target_url + "&type=gitlab"
        }
        redisData["url"] = target_url
        redisData["pipeline_status"] = pipeline.Status
    }

    err = json.NewEncoder(w).Encode(redisData)
    pie(logger.Error, err, "Failed encoding data to json", http.StatusInternalServerError)
}
