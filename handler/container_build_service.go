package handler

import (
    "fmt"
    "io"
    "net/http"
    "os"
    "os/exec"
    "strings"
    "time"

    "github.com/MicahParks/keyfunc/v2"
    "github.com/go-git/go-git/v5"
    gitconfig "github.com/go-git/go-git/v5/config"
    "github.com/go-git/go-git/v5/plumbing/object"
    "github.com/golang-jwt/jwt/v5"
    "github.com/xanzy/go-gitlab"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

var jwks_keyfunc_api_gw *keyfunc.JWKS = nil
func PrepareJwksKeyfuncApiGw(url string) {
    options := keyfunc.Options {
        RefreshInterval: time.Hour*24,
        RefreshRateLimit: time.Minute*1,
        RefreshUnknownKID: true,
    }

    var err error
    if jwks_keyfunc_api_gw, err = keyfunc.Get(url, options); err != nil {
        panic(fmt.Sprintf("Error while fetching JWKS certificates. err=%v", err))
    }
}

// returns the socpes as set in the JWT (whitespace separated list of words)
// a JWT is created for different scopes, and endpoints should reject a JWT,
// if they do not match their scope
func validate_jwt(token string) ([]string, error) {
    var ret []string
    if access_token, err := jwt.Parse(token, jwks_keyfunc_api_gw.Keyfunc); err != nil {
        return nil, err
    } else {
        mapclaims := access_token.Claims.(jwt.MapClaims)
        if scope, exists := mapclaims["scope"]; exists == false {
            logging.Errorf(condition_error{"JWT claims did not include scope"}, "JWT claims did not include scope. All claims=%#v", mapclaims)
        } else {
            ret = strings.Split(scope.(string), " ")
        }
    }
    return ret, nil
}

func GetContainerBuildHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(containerBuild{glserver, db, config})
}

type containerBuild struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
}

// body must be the Dockerfile to build - will redirect to a website, which has the results
func (h containerBuild) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    token := strings.TrimSpace(strings.Replace(r.Header.Get("Authorization"), "Bearer", "", 1))
    scopes, err := validate_jwt(token)
    pie(logger.Error, err, "Authorization failed", http.StatusUnauthorized)

    if util.SliceContains(scopes, "ciext") == false {
        logger.Warn().Msg("Could not find scope 'ciext' in JWT. This is not yet a hard error.")
    }

    pipeline_id := h.config.Gitlab.ContainerBuilderServicePipelineId
    repo_id := h.db.GetRepositoryId(pipeline_id)
    new_branch := fmt.Sprintf("%v-%v", logging.GetReqCorrelationID(r), pipeline_id)
    git_target_path := fmt.Sprintf("/home/tmp/container_build/%v", new_branch)
    arch := r.URL.Query().Get("arch")
    variables := map[string]string{
        "PERSIST_IMAGE_NAME": fmt.Sprintf("$CSCS_REGISTRY_PATH/public/%v:latest", new_branch),
        "ARCH": arch,
    }

    // check if arch has a valid value
    if arch != "aarch64" && arch != "x86_64" {
        pie(logger.Error, condition_error{"Query parameter 'arch' must be either aarch64 or x86_64"}, "", http.StatusBadRequest)
    }

    // custom image name mode - check for all required fields
    if r.URL.Query().Get("image") != "" {
        variables["PERSIST_IMAGE_NAME"] = r.URL.Query().Get("image")
        if strings.HasPrefix(variables["PERSIST_IMAGE_NAME"], h.config.RegistryPath) {
            pie(logger.Error, condition_error{"Custom image name must not be in internal repository"}, "", http.StatusBadRequest)
        }
        variables["CUSTOM_REGISTRY_USERNAME"] = r.Header.Get("X-Registry-Username")
        variables["CUSTOM_REGISTRY_PASSWORD"] = r.Header.Get("X-Registry-Password")
        if variables["CUSTOM_REGISTRY_USERNAME"] == "" || variables["CUSTOM_REGISTRY_PASSWORD"] == "" {
            errMsg := "Headers X-Registry-Username and X-Registry-Password must not be empty when query parameter `image` is set"
            pie(logger.Error, condition_error{errMsg}, "", http.StatusBadRequest)
        }
    }


    util.LockRepoDir(git_target_path, logger)
    defer util.UnlockRepoDir(git_target_path, logger)

    repo, err := util.GitCloneOrOpen(&h.db, &h.config, git_target_path, repo_id, false, logger, nil)
    pie(logger.Error, err, "Failed clonging/opening the container build project", http.StatusInternalServerError)

    err = util.GitFetch(&h.db, &h.config, repo, "", []gitconfig.RefSpec{gitconfig.RefSpec("+refs/heads/main:refs/heads/main")}, repo_id, logger, nil)
    pie(logger.Error, err, "Failed fetching main branch for container build project", http.StatusInternalServerError)

    err = util.EnsureMirrorRepo(&h.db, &h.config, h.glserver, logger, repo_id, pipeline_id)
    pie(logger.Error, err, "Failed creating mirror repository", http.StatusInternalServerError)

    worktree, err := repo.Worktree()
    pie(logger.Error, err, "Failed opening worktree from repo", http.StatusInternalServerError)

    // two modes
    // 1. body is only the dockerfile
    // 2. body is a full tar archive, which is the build context and a dockerfile inside, specified by path in the query parameter `dockerfile`
    if r.URL.Query().Get("dockerfile") == "" {
        // mode 1 - body is expected to only be a simple dockerfile
        dockerfile_content, err := io.ReadAll(r.Body)
        pie(logger.Error, err, "Failed getting dockerfile from POST event", http.StatusBadRequest)
        err = os.WriteFile(fmt.Sprintf("%v/ci/Dockerfile", git_target_path), dockerfile_content, 0600)
        pie(logger.Error, err, "Failed writing Dockerfile", http.StatusInternalServerError)
        _, err = worktree.Add("ci/Dockerfile")
        pie(logger.Error, err, "Failed adding new Dockerfile to repository worktree", http.StatusInternalServerError)
    } else {
        // mode 2 - body is a full tar.gz archive (we only support tar.gz!)
        // 1. extract the full body (ignoring the file ci/cscs.yml, because this is owned by us)
        // 2. add all new files/directories to repo
        // extract
        tarCmd := exec.Command("tar",
            "-xzf", "-",
            "--directory="+git_target_path,
            "--exclude=./ci/cscs.yml") // #nosec G204 - golang escapes arguments, no injection possible
        tarCmd.Stdout = w
        tarCmd.Stderr = w
        tarCmd.Stdin = r.Body
        err := tarCmd.Run()
        pie(logger.Error, err, "Failed extracting build context", http.StatusBadRequest)

        // add new files/directories
        status, err := worktree.Status()
        pie(logger.Error, err, "Failed getting status of repository", http.StatusInternalServerError)
        for filename, filestatus := range status {
            if filestatus.Worktree == git.Untracked || filestatus.Worktree == git.Modified {
                _, err := worktree.Add(filename)
                pie(logger.Error, err, "Failed adding file to build context", http.StatusInternalServerError)
            }
        }

        // ensure that the pipeline picks up the correct dockerfile
        variables["DOCKERFILE"] = r.URL.Query().Get("dockerfile")

        if _, err := os.Stat(fmt.Sprintf("%v/%v", git_target_path, variables["DOCKERFILE"])); err != nil {
            pie(logger.Error, err, "Could not find the dockerfile", http.StatusBadRequest)
        }
    }

    commit, err := worktree.Commit("update", &git.CommitOptions{
        Author: &object.Signature{Name: "CSCS CI-Ext", Email: "ciext-admin@cscs.ch", When: time.Now()},
    })
    pie(logger.Error, err, "failed commiting to worktree", http.StatusInternalServerError)

    refspecs := []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("+refs/heads/main:refs/heads/%v", new_branch))}
    err = util.GitPush(&h.db, &h.config, repo, refspecs, repo_id, pipeline_id, logger, nil)
    pie(logger.Error, err, "Failed pushing changes to mirror repository", http.StatusInternalServerError)

    _, err = util.TriggerPipelineWithRetry(h.glserver, &h.config, &h.db, pipeline_id, new_branch, commit.String(), variables, logger)
    pie(logger.Error, err, fmt.Sprintf("Failed triggering pipeline with err=%v", err), http.StatusBadRequest)

    _,_ = w.Write([]byte("Building container image "))
    _,_ = fmt.Fprintf(w, "and pushing the result to %v\n", variables["PERSIST_IMAGE_NAME"])
    _,_ = fmt.Fprintf(w, "For build logs please visit %v\n", util.BuildCIURL("/%v?image=%v", &h.config, r.URL.Path, new_branch))
}

func (h containerBuild) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    branch_name := r.URL.Query().Get("image")
    if branch_name == "" {
        pie(logger.Error, condition_error{"Parameter image is empty"}, "Parameter image is empty", http.StatusBadRequest)
    }
    pipeline_id := h.config.Gitlab.ContainerBuilderServicePipelineId
    repo_id := h.db.GetRepositoryId(pipeline_id)
    mirror_project := fmt.Sprintf("%v/%v/%v", h.config.Gitlab.MirrorsPath, repo_id, pipeline_id)
    pipelines, _, err := h.glserver.Pipelines.ListProjectPipelines(mirror_project, &gitlab.ListProjectPipelinesOptions{Ref: &branch_name})
    pie(logger.Error, err, "Failed getting pipeline", http.StatusBadRequest)
    var pipeline *gitlab.PipelineInfo
    for _, p := range pipelines {
        if p.Status != util.GlStatusSkipped {
            pipeline = p
        }
    }
    if pipeline == nil {
        pie(logger.Error, condition_error{"Could not find pipeline with this name"}, "", http.StatusBadRequest)
    }

    jobs, _, err := h.glserver.Jobs.ListPipelineJobs(mirror_project, pipeline.ID, &gitlab.ListJobsOptions{IncludeRetried: gitlab.Bool(false)})
    pie(logger.Error, err, "Failed getting pipeline's jobs", http.StatusBadRequest)

    redirect_to := util.BuildCIURL("/job/result/%v/%v/%v", &h.config, pipeline_id, pipeline.ProjectID, jobs[0].ID)
    http.Redirect(w, r, redirect_to, http.StatusFound)
}
