package handler

import (
    "net/http"
    "strconv"

    "github.com/xanzy/go-gitlab"
    "gopkg.in/yaml.v3"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetJobRestartHandler(glserver *gitlab.Client, db util.DB) func(w http.ResponseWriter, r *http.Request) {
    return wrap(jobRestartHandler{glserver, db})
}
func GetJobCancelHandler(glserver *gitlab.Client, db util.DB) func(w http.ResponseWriter, r *http.Request) {
    return wrap(jobCancelHandler{glserver, db})
}
func GetJobPlayHandler(glserver *gitlab.Client, db util.DB) func(w http.ResponseWriter, r *http.Request) {
    return wrap(jobPlayHandler{jobRestartHandler: jobRestartHandler{glserver, db}})
}
func GetPipelineRestartHandler(glserver *gitlab.Client, db util.DB) func(w http.ResponseWriter, r *http.Request) {
    return wrap(pipelineRestartHandler{glserver, db})
}
func GetPipelineCanceltHandler(glserver *gitlab.Client, db util.DB) func(w http.ResponseWriter, r *http.Request) {
    return wrap(pipelineCancelHandler{glserver, db})
}

type jobRestartHandler struct {
    Glserver *gitlab.Client
    DB util.DB
}

type jobCancelHandler jobRestartHandler
type jobPlayHandler struct {
    jobRestartHandler
    playJobOptions *gitlab.PlayJobOptions
}
type pipelineRestartHandler jobRestartHandler
type pipelineCancelHandler jobRestartHandler


func restart_cancel_job_helper[T any](w http.ResponseWriter, r* http.Request, db util.DB, formfield string, glfunc func(interface{}, int, ...gitlab.RequestOptionFunc)(*T, *gitlab.Response, error)) {
    logger := logging.GetReqLogger(r)

    // check if we are authorized to accept this request
    var authorized bool
    var claims []string
    if authorized, claims = is_authorized(r, w, false); authorized == false {
        http.Error(w, "You are not authorized, please login first", http.StatusForbidden)
        return
    }

    // we are authorized - check if we have manager permissions
    pipeline_id, err := strconv.ParseInt(r.FormValue("pipelineid"), 10, 64)
    pie(logger.Error, err, "Invalid value provided for pipelineid", http.StatusBadRequest)
    repo_id := db.GetRepositoryId(pipeline_id)
    if db.GetAccessLevelForRepoWithClaims(repo_id, claims) < util.ManagerAccess {
        http.Error(w, "You do not have manager permissions for this repository", http.StatusForbidden)
        return
    }

    id_to_restart, err := strconv.Atoi(r.FormValue(formfield))
    pie(logger.Error, err, "Invalid value provided for "+formfield, http.StatusBadRequest)
    projectid := r.FormValue("projectid")
    if projectid == "" {
        http.Error(w, "Received projectid is empty - Nothing to restart", http.StatusBadRequest)
        return
    }
    _, resp, err := glfunc(projectid, id_to_restart)
    pie(logger.Error, err, "Failed calling job API call", resp.StatusCode)

    loc := r.Header.Get("Referer")
    if loc == "" {
        loc = r.FormValue("redirect")
    }
    http.Redirect(w, r, loc, http.StatusFound)
}

func (h jobRestartHandler) Post(w http.ResponseWriter, r *http.Request) {
    restart_cancel_job_helper(w, r, h.DB, "jobid", h.Glserver.Jobs.RetryJob)
}

func (h jobCancelHandler) Post(w http.ResponseWriter, r *http.Request) {
    // see job_restart.go for the implementation
    restart_cancel_job_helper(w, r, h.DB, "jobid", h.Glserver.Jobs.CancelJob)
}

func (h pipelineRestartHandler) Post(w http.ResponseWriter, r *http.Request) {
    restart_cancel_job_helper(w, r, h.DB, "gitlab_pipelineid", h.Glserver.Pipelines.RetryPipelineBuild)
}

func (h pipelineCancelHandler) Post(w http.ResponseWriter, r *http.Request) {
    restart_cancel_job_helper(w, r, h.DB, "gitlab_pipelineid", h.Glserver.Pipelines.CancelPipelineBuild)
}

func (h jobPlayHandler) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    varMap := make(map[string]string)
    err := yaml.Unmarshal([]byte(r.FormValue("jobvars")), &varMap)
    pie(logger.Error, err, "Failed decoding yaml", http.StatusBadRequest)
    jobvarsoptions := []*gitlab.JobVariableOptions{}
    for key, value := range varMap {
        jobvarsoptions = append(jobvarsoptions, &gitlab.JobVariableOptions{Key: gitlab.String(key), Value: gitlab.String(value)})
    }
    h.playJobOptions = &gitlab.PlayJobOptions{JobVariablesAttributes: &jobvarsoptions}

    restart_cancel_job_helper(w, r, h.DB, "jobid", h.CallPlayFunc)
}

func (h jobPlayHandler) CallPlayFunc(pid interface{}, jid int, options ...gitlab.RequestOptionFunc) (*gitlab.Job, *gitlab.Response, error) {
    return h.Glserver.Jobs.PlayJob(pid, jid, h.playJobOptions, options...)
}
