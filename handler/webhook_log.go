package handler

import (
    "bytes"
    "fmt"
    "html/template"
    "net/http"
    "os"

    "cscs.ch/cicd-ext-mw/thirdparty/ansihtml"
    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetWebhookLogHandler(config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(webhookLogHandler{config: config})
}

type webhookLogHandler struct {
    config util.Config
}

func (h webhookLogHandler) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    reqId := r.URL.Query().Get("reqId")
    // nosemgrep: gosec.G304-1
    logFile, err := os.Open(h.config.GetRequestLogPath(reqId))
    pie(logger.Error, err, "Could not find log file", http.StatusBadRequest)
    html_out := new(bytes.Buffer)
    ansihtml.ConvertToHTML(logFile, html_out)

    // we know that html_out is already HTML, so we should not escape it anymore - it will be inserted as as in the template
    tmpl_data := webhookLogTmplData{
        Title: fmt.Sprintf("Log for request %v", reqId),
        // nosemgrep: gosec.G203-1
        ReqLog: template.HTML(html_out.String()), // #nosec G203
        UrlPrefix: h.config.URLPrefix,
    }

    err = util.ExecuteTemplate(w, "webhook_log.html", &tmpl_data)
    pie(logger.Error, err, "Error rendering template webhook_log.html", http.StatusInternalServerError)
}

type webhookLogTmplData struct {
    Title string
    ReqLog template.HTML
    UrlPrefix string
}
