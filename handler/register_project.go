package handler
import (
    "fmt"
    "net/http"
    "strings"

    "github.com/snapcore/snapd/randutil"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetRegisterProjectHandler(db *util.DB, config *util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(registerProjectHandler{db, config})
}

type registerProjectHandler struct {
    DB *util.DB
    Config *util.Config
}


func (h registerProjectHandler) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    can_register, claims := can_register_project(w, r, h.Config)
    if can_register == false {
        pie(logger.Error, condition_error{"You are not allowed to register a project"}, "", http.StatusForbidden)
    }
    tmpl_data := registerProjectTmplData{
        UrlPrefix: h.Config.URLPrefix,
        Favicon: "setup-favicon.svg",
    }
    if len(claims) > 0 {
        tmpl_data.LoggedInUser = claims[0]
    }

    err := util.ExecuteTemplate(w, "register_project.html", &tmpl_data)
    pie(logger.Error, err, "Error rendering template register_project.html", http.StatusInternalServerError)
}


func (h registerProjectHandler) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    if can_register, _ := can_register_project(w, r, h.Config); can_register == false {
        pie(logger.Error, condition_error{"You are not allowed to register a project"}, "", http.StatusForbidden)
    }

    err := r.ParseForm()
    pie(logger.Error, err, "Failed parsing form", http.StatusBadRequest)

    repo_url := r.FormValue("url")
    name := r.FormValue("name")
    owner := r.FormValue("owner")
    private_repo := r.FormValue("private") != ""

    // remove .git suffix on repo url
    repo_url, _ = strings.CutSuffix(repo_url, ".git")

    // Fail registration if repository url is already registered
    if h.DB.FindRepo(repo_url) > 0 {
        pie(logger.Error, condition_error{"This repository is already registered"}, "", http.StatusBadRequest)
    }

    project_id, err := h.DB.AddProject(name)
    pie(logger.Error, err, "Failed adding project to DB", http.StatusInternalServerError)

    webhook_secret, err := randutil.CryptoToken(16)
    pie(logger.Error, err, "Failed generating webhook secret", http.StatusInternalServerError)
    repo_id, err := h.DB.AddRepository(project_id, repo_url, name, owner, webhook_secret, private_repo)
    pie(logger.Error, err, "Failed adding repository to DB", http.StatusInternalServerError)

    pipeline_id, err := h.DB.AddPipeline(repo_id, "default")
    pie(logger.Error, err, "Failed adding pipeline to DB", http.StatusInternalServerError)
    h.DB.UpdateCIEntrypoint(pipeline_id, "ci/cscs.yml")

    repo_allowance := ""
    if repo_path_split := strings.Split(h.Config.RegistryPath, "/"); len(repo_path_split)>1 {
        repo_allowance = strings.Join(repo_path_split[1:], "/")
    }
    err = h.DB.AddRegistryAllowPaths(repo_id, repo_allowance, fmt.Sprintf("%v/**", repo_id))
    pie(logger.Error, err, "Failed adding registry allowance to DB", http.StatusInternalServerError)
    err = h.DB.AddRegistryAllowPaths(repo_id, repo_allowance, "**/public/**")
    pie(logger.Error, err, "Failed adding registry allowance to DB", http.StatusInternalServerError)

    _, err = util.CreateSshKeys(h.Config.GetSSHKeyPath(repo_id, false, false))
    pie(logger.Error, err, "Failed generating SSH keys", http.StatusInternalServerError)

    _,_ = fmt.Fprintf(w, "Webhook-Secret: %v", webhook_secret)
}


func can_register_project(w http.ResponseWriter, r *http.Request, config *util.Config) (bool, []string) {
    allowed_to_register := false
    if authorized, claims := is_authorized(r, w, false); authorized {
        for _, allowed_claim := range config.ProjectRegistration {
            if util.SliceContains(claims, allowed_claim) {
                allowed_to_register = true
                break
            }
        }
        return allowed_to_register, claims
    }
    return false, nil
}

type registerProjectTmplData struct {
    LoggedInUser string
    UrlPrefix string
    Favicon string
}
