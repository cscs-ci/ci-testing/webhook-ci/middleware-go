package handler

import (
    "io/ioutil"
    "net/http"

    "cscs.ch/cicd-ext-mw/logging"
)

type root struct{}
func (h root) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    logger.Debug().Msg("Get")
    _, _ = w.Write([]byte("root.Get!\n"))
}
func (h root) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    body, err := ioutil.ReadAll(r.Body)
    if err != nil {
        http.Error(w, "Error reading body", http.StatusBadRequest)
    }

    logger.Debug().Msgf("Post with body=%s", body)

    _, _ = w.Write([]byte("root.Post!\n"))
}

func GetRootHandler() func(w http.ResponseWriter, r *http.Request) {
    return wrap(root{})
}
