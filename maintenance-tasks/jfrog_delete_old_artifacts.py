import argparse
import datetime
import logging
import pprint
import re
import requests
import time

import config

# enable DEBUG logging also for urllib3 to see requests/responses that are sent/received to/from the server
#import http.client
#http.client.HTTPConnection.debuglevel = 1
#logging.basicConfig()
#logging.getLogger().setLevel(logging.DEBUG)
#requests_log = logging.getLogger("urllib3")
#requests_log.setLevel(logging.DEBUG)
#requests_log.propagate = True
## End of DEBUG settings for full network debugging

publicAndBaseimageRetention = 24*3600*365 # keep baseimage and public images for 365 days after last download
deleteAfter = b'1months' # delete generally images after one month without download
repoQuota = 300 * 1024*1024*1024 # free quota until 300GB has been reached
pathMatch = b'*'
repository = b''
repositoryS = ''
apikey = ''
base = ''

def getRepoSize(uri: str) -> int:
    r = requests.get(f'{base}/artifactory/api/storage/{repositoryS}{repo["uri"]}?list&deep=1&listFolders=0&', headers={'X-JFrog-Art-Api': apikey}, timeout=600)
    r.raise_for_status()
    totalSize: int = sum([f['size'] for f in r.json()['files'] if f['size']>0])
    return totalSize

def deleteFromPath(uri: str, includeBaseDeploy: bool) -> None:
    search_spec = b'''items.find({
            "repo":{"$eq":"REPOSITORY"},
            "name":{"$eq":"manifest.json"},
            "path":{"$match":"PATHMATCH"},
            "$or": [
                {
                    "stat.downloaded":{"$before":"DELETEAFTER"}
                },
                {
                    "$and": [
                        {"stat.downloads":{"$eq":null}},
                        {"created":{"$before": "DELETEAFTER"}}
                    ]
                }
            ]
        }).include("name", "stat.downloads", "stat.downloaded", "repo", "path", "created")'''
    this_search = search_spec.replace(b'DELETEAFTER', deleteAfter) \
                             .replace(b'REPOSITORY', repository) \
                             .replace(b'PATHMATCH', uri.encode('ascii'))
    r = requests.post(f'{base}/artifactory/api/search/aql', headers={'X-JFrog-Art-Api': apikey}, data=this_search, timeout=600)
    r.raise_for_status()
    for item in r.json()['results']:
        if len(item['stats']) != 1:
            raise RuntimeError("We expect the field 'stats' to be an arry of exactly length 1")
        shouldDelete = False
        stats = item['stats'][0]
        path = item['path']
        if stats['downloads'] == 0:
            shouldDelete = True
        else:
            if (path.find('base/') != -1 or
                path.find('baseimage/') != -1 or
                path.find('baseimg/') != -1 or
                path.find('deploy/') != -1 or
                path.find('deployment/') != -1 ) \
                and datetime.datetime.now().timestamp()-datetime.datetime.fromisoformat(stats['downloaded']).timestamp() > publicAndBaseimageRetention:
                    if includeBaseDeploy:
                        shouldDelete = True
            else:
                shouldDelete = True
        if shouldDelete:
            repo = item['repo']
            path = item['path']
            r = requests.delete(f'{base}/artifactory/{repo}/{path}/', headers={'X-JFrog-Art-Api': apikey}, timeout=600)
            try:
                r.raise_for_status()
            except Exception as e:
                print(f'Failed deleting artifact. Exception={e}. r.text={r.text}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', required=True)
    args = parser.parse_args()
    cfg = config.Config(args.config)
    apikey = cfg.jfrogPassword()
    base = cfg.jfrogUrl()
    repositoryS = cfg.jfrogRepoDir()
    repository = repositoryS.encode('ascii')

    r = requests.get(f'{base}/artifactory/api/storage/{repositoryS}?list&depth=1&listFolders=1', headers={'X-JFrog-Art-Api': apikey}, timeout=600)
    r.raise_for_status()
    files = r.json()['files']
    for repo in files:
        if repo['folder'] and re.search('^/[1-9]', repo['uri']):
            # top-level repo
            repoSize = getRepoSize(repo["uri"])
            repoSizeBeforeCleanup = repoSize
            if repoSize > repoQuota:
                deleteFromPath(repo["uri"][1:]+"/*", includeBaseDeploy=False)
                repoSize = getRepoSize(repo["uri"])
            if repoSize > repoQuota:
                deleteFromPath(repo["uri"][1:]+"/*", includeBaseDeploy=True)
                repoSize = getRepoSize(repo["uri"])
            if repoSize > repoQuota:
                print(f'Repo {repo["uri"]} size before cleanup={repoSizeBeforeCleanup/1024/1024/1024:1.2f}GB and after cleanup={repoSize/1024/1024/1024:1.2f}GB')

    # delete from trashcan
    r = requests.delete(f'{base}/artifactory/api/trash/clean/{repositoryS}', headers={'X-JFrog-Art-Api': apikey}, timeout=600)
    try:
        r.raise_for_status()
    except Exception as e:
        print(f'Failed cleaning trashcan. Exception={e}. r.text={r.text}')
