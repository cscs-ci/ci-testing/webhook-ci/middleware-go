ARG BASE_IMAGE
FROM $BASE_IMAGE as builder

# Check ssh fingerprints
COPY known_hosts /etc/ssh/ssh_known_hosts
COPY known_hosts.urls /etc/ssh/ssh_known_hosts.urls
RUN for host in $(cat /etc/ssh/ssh_known_hosts.urls) ; do echo "Verifying ssh key for host=$host" ; if ! ssh-keygen -F $host -f /etc/ssh/ssh_known_hosts ; then echo "Could not verify ssh key for host=$host" ; false ; fi ; done

# build sources
COPY . /sources
WORKDIR /sources
RUN go build ./apps/server
RUN go build ./apps/background_tasks

# Final container - copy from builder container
FROM ubuntu:22.04

RUN apt update && apt upgrade -y && env DEBIAN_FRONTEND=noninteractive apt install -y  tzdata ca-certificates skopeo git && rm -Rf /var/lib/apt/lists/*

# go server component
COPY --from=builder /sources/server /usr/local/bin
COPY --from=builder /sources/background_tasks /usr/local/bin
COPY --from=builder --chmod=644 /etc/ssh/ssh_known_hosts /etc/ssh/ssh_known_hosts
COPY apps/server/templates /opt/ci_go/templates
COPY apps/server/static    /opt/ci_go/static

# filebeat
RUN mkdir -p /opt/filebeat
COPY --from=builder /tmp/filebeat /opt/filebeat/filebeat
