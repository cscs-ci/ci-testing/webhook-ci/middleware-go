package util

import (
    "encoding/json"
    "fmt"
    "time"

    "github.com/gophercloud/gophercloud"
    "github.com/gophercloud/gophercloud/openstack/identity/v3/extensions/ec2credentials"
    "github.com/gophercloud/utils/openstack/clientconfig"
    "github.com/snapcore/snapd/randutil"
    "github.com/go-playground/retry"

    "cscs.ch/cicd-ext-mw/logging"
)

const SystemJFrogUser = "jfrog.user"
const SystemJFrogPerm = "jfrog.perm"
const SystemCastor    = "castor"

var castorAuthOpts *gophercloud.AuthOptions = nil
var castorClient   *gophercloud.ServiceClient = nil

type userCreate struct {
    Username string               `json:"username"`
    Password string               `json:"password"`
    EMail string                  `json:"email"`
    Groups []string               `json:"groups"`
    Realm string                  `json:"realm,omitempty"`
    Status string                 `json:"status,omitempty"`
    Admin bool                    `json:"admin"`
    ProfileUpdatable bool         `json:"profile_updatable"`
    InternalPasswordDisabled bool `json:"internal_password_disabled"`
    DisableUiAccess bool          `json:"disable_ui_access"`
}
func newUserCreate(username, password, email string) userCreate {
    return userCreate{
        Username: username,
        Password: password,
        EMail: email,
        Groups: []string{},
        Admin: false,
        ProfileUpdatable: false,
        InternalPasswordDisabled: false,
        DisableUiAccess: true,
    }
}
type permissionsCreateAccessActions struct {
    Users map[string][]string `json:"users,omitempty"`
    Groups map[string][]string `json:"groups,omitempty"`
}
type permissionCreateAccess struct {
    Actions *permissionsCreateAccessActions `json:"actions,omitempty"`
    Repositories []string `json:"repositories"`
    IncludePatterns []string `json:"include-patterns,omitempty"`
    ExcludePatterns []string `json:"exclude-patterns,omitempty"`
}
type permissionCreate struct {
    Name string `json:"name,omitempty"`
    Repo *permissionCreateAccess `json:"repo,omitempty"`
    Build *permissionCreateAccess `json:"build,omitempty"`
    ReleaseBundle *permissionCreateAccess `json:"releaseBundle"`
}
func newPermissionCreate(username, repo string, include_patterns []string) permissionCreate {
    return permissionCreate{
        Repo: &permissionCreateAccess{
            Actions: &permissionsCreateAccessActions{
                Users: map[string][]string {username: []string{"read","annotate","write","delete"}},
            },
            Repositories: []string{repo},
            IncludePatterns: include_patterns,
        },
    }
}

// creates a new user in JFrog, will
// token must have the permissions to create a user
func CreateJFrogUser(username, email, jfrog_url, token string) (string, error) {
    // We interact with the REST-API directly instead of using the jfrog-client-go
    // The reason is that the client is not up to date, e.g. creating users is using the old API endpoint
    password, err := randutil.CryptoToken(16)
    if err != nil {
        return "", err
    }
    password += "Ab1_" // ensure that we fulfil password requirements

    headers := map[string]string {
        "Authorization": fmt.Sprintf("Bearer %v", token),
    }
    resp, err := DoJsonRequest("POST",
        fmt.Sprintf("%v/access/api/v2/users", jfrog_url),
        headers,
        newUserCreate(username, password, email))
    if resp.StatusCode == 409 {
        // user exists already, update password
        logging.Debugf("Failed creating new user in JFrog. err=%v. resp.StatusCode==409 --> Retrying to patch user", err)
        resp, err = DoJsonRequest("PATCH",
            fmt.Sprintf("%v/access/api/v2/users/%v", jfrog_url, username),
            headers,
            map[string]string{"password":password},
        )
    }
    if err != nil {
        return "", err
    }
    var user_create_reply userCreate
    if err := CheckResponse(resp); err != nil {
        return "", err
    }
    if err := json.Unmarshal(resp.ResponseData, &user_create_reply); err != nil {
        return "", err
    }

    // user is now in the system, but it is part of all default groups - remove the user again from all default groups
    resp, err = DoJsonRequest("PATCH",
        fmt.Sprintf("%v/access/api/v2/users/%v/groups", jfrog_url, username),
        headers,
        map[string][]string{"remove": user_create_reply.Groups, "add": []string{"ci-ext"}})
    if err != nil {
        return "", err
    }
    if err := CheckResponse(resp); err != nil {
        return "", err
    }
    return password,nil
}


func DeleteJFrogUser(username, jfrog_url, token string) error {
    headers := map[string]string {
        "Authorization": fmt.Sprintf("Bearer %v", token),
    }
    resp, err := DoJsonRequest("DELETE",
        fmt.Sprintf("%v/access/api/v2/users/%v", jfrog_url, username),
        headers,
        nil)
    if err != nil {
        return err
    }
    if err := CheckResponse(resp); err != nil {
        return err
    }
    return nil
}


// creates a new permission in JFrog, which gives access to `repo` with `include_patterns` to `username`.
// The permission will be called `permission_name
// Call multiple times if you want to give access to different repositories in JFrog
//    technically the JFrog API would allow to create a single permission with multiple repositories, but the include_patterns applies to
//    all repositories, i.e. it could happen that we give access to more paths than intended
func CreateJFrogPermission(permission_name, username, repo string, include_patterns []string, jfrog_url, token string) error {
    headers := map[string]string {
        "Authorization": fmt.Sprintf("Bearer %v", token),
    }
    resp, err := DoJsonRequest("POST",
        fmt.Sprintf("%v/artifactory/api/v2/security/permissions/%v", jfrog_url,  permission_name),
        headers,
        newPermissionCreate(username, repo, include_patterns))
    if err != nil {
        return err
    }
    if err := CheckResponse(resp); err != nil {
        return err
    }
    return nil
}

func DeleteJFrogPermission(permission_name, jfrog_url, token string) error {
    headers := map[string]string {
        "Authorization": fmt.Sprintf("Bearer %v", token),
    }
    resp, err := DoJsonRequest("DELETE",
        fmt.Sprintf("%v/artifactory/api/v2/security/permissions/%v", jfrog_url, permission_name),
        headers,
        nil)
    if err != nil {
        return err
    }
    if err := CheckResponse(resp); err != nil {
        return err
    }
    return nil
}


func ensureCastorInitialized(cloud string) error {
    var err error
    if castorAuthOpts == nil {
        opts := new(clientconfig.ClientOpts)
        opts.Cloud = cloud
        castorAuthOpts, err = clientconfig.AuthOptions(opts)
        if err != nil {
            return err
        }
    }
    if castorClient == nil {
        opts := new(clientconfig.ClientOpts)
        opts.Cloud = cloud
        castorClient, err = clientconfig.NewServiceClient("identity", opts)
        if err != nil {
            return err
        }
    }
    return nil
}

func CreateCastorEC2Credentials(cloud string) (string, string, error) {
    if err := ensureCastorInitialized(cloud); err != nil {
        return "", "", err
    }
    create_opts := ec2credentials.CreateOpts{TenantID: castorAuthOpts.TenantID}

    var creds *ec2credentials.Credential
    err := retry.Run(3, func() (bool, error) {
        var err error
        if creds, err = ec2credentials.Create(castorClient, castorAuthOpts.UserID, create_opts).Extract(); err != nil {
            return false, err
        }
        return false, nil
    }, func(attempt uint16, err error) {
        if attempt > 1 {
            // one attempt is expected when the castorClient times out with its internal credentials (about 24h after creation)
            logging.Warnf("CreateCastorEC2Credentials failed with err=%v attempt=%v - reinitializing castorClient", err, attempt)
        }
        castorAuthOpts = nil
        castorClient = nil
        time.Sleep(time.Duration(attempt-1)*time.Second)
        _ = ensureCastorInitialized(cloud)
    })
    if err != nil {
        return "", "", err
    }
    logging.Debugf("Created ec2 creds=%+v", creds)
    return creds.Access, creds.Secret, nil
}

func DeleteCastorEC2Credentials(cloud, access_id string) error {
    if err := ensureCastorInitialized(cloud); err != nil {
        return err
    }
    err := retry.Run(3, func() (bool, error) {
        if res := ec2credentials.Delete(castorClient, castorAuthOpts.UserID, access_id); res.Err != nil {
            return false, res.Err
        }
        return false, nil
    }, func(attempt uint16, err error) {
        if attempt > 1 {
                // one attempt is expected when the castorClient times out with its internal credentials (about 24h after creation)
            logging.Warnf("DeleteCastorEC2Credentials failed with err=%v attempt=%v - reinitializing castorClient", err, attempt)
        }
        castorAuthOpts = nil
        castorClient = nil
        time.Sleep(time.Duration(attempt-1)*time.Second)
        _ = ensureCastorInitialized(cloud)
    })
    return err
}
