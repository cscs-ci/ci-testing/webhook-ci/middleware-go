package util

import (
    "fmt"
    "strings"
    "sync"
    "time"

    "cscs.ch/cicd-ext-mw/logging"
    "github.com/xanzy/go-gitlab"
)


type TokenDeleter struct {
    glserver *gitlab.Client
    db DB
    config Config
}

var onceTokenDeleter sync.Once
var td TokenDeleter
var tdGlserver *gitlab.Client
var tdDB DB
var tdConfig Config

func SetTokenDeleterParams(glserver *gitlab.Client, db DB, config Config) {
    tdGlserver = glserver
    tdDB = db
    tdConfig = config
}

// make sure that you first call SetTokenDeleterParams(...)
func GetTokenDeleter() *TokenDeleter {
    onceTokenDeleter.Do(func() {
        td = NewTokenDeleter(tdGlserver, tdDB, tdConfig)
    })

    return &td
}

func NewTokenDeleter(glserver *gitlab.Client, db DB, config Config) TokenDeleter {
    return TokenDeleter{glserver: glserver, db: db, config: config}
}


var tokenDeleteFailures = map[string]int{}
func (td TokenDeleter) DeleteToken(cred Credential) {
    logging.Debugf("Deleting token for job_id=%v, system=%v and username=%v", cred.JobID, cred.System, cred.Username)
    if cred.System == SystemJFrogUser {
        if err := DeleteJFrogUser(cred.Username, td.config.JFrog.URL, td.config.JFrog.Token); err != nil {
            logging.Errorf(err, "Failed deleting JFrog user %v", cred.Username)
            tokenDeleteFailures[cred.Username] += 1
            if tokenDeleteFailures[cred.Username] < 10 {
                return
            } else {
                logging.Warnf("Failed 10 times trying to delete token with username=%v. Giving up to retry", cred.Username)
            }
        }
    } else if strings.HasPrefix(cred.System, SystemJFrogPerm) {
        if err := DeleteJFrogPermission(cred.Username, td.config.JFrog.URL, td.config.JFrog.Token); err != nil {
            logging.Errorf(err, "Failed deleting JFrog permission %v", cred.Username)
            tokenDeleteFailures[cred.Username] += 1
            if tokenDeleteFailures[cred.Username] < 10 {
                return
            } else {
                logging.Warnf("Failed 10 times trying to delete token with username=%v. Giving up to retry", cred.Username)
            }
        }
    } else if cred.System == SystemCastor {
        if err := DeleteCastorEC2Credentials(td.config.SpackBuildcache.Cloud, cred.Username); err != nil {
            logging.Errorf(err, "Failed deleting castor EC2 permission %v for job_id=%v", cred.Username, cred.JobID)
            tokenDeleteFailures[cred.Username] += 1
            if tokenDeleteFailures[cred.Username] < 10 {
                return
            } else {
                logging.Warnf("Failed 10 times trying to delete token with username=%v. Giving up to retry", cred.Username)
            }
        }
    } else {
        logging.Errorf(fmt.Errorf("Unknown token system=%v", cred.System), "Cannot delete credential due to unknown system")
        return
    }
    logging.Debugf("Successfully deleted token for job_id=%v, system=%v and username=%v", cred.JobID, cred.System, cred.Username)
    if err := td.db.DeleteCredential(cred.JobID, cred.System); err != nil {
        logging.Errorf(err, "Failed deleting credential from database")
    }
}


// watch periodically for data in database and delete tokens when the job has finished
func (td TokenDeleter) Watch() {
    // we want to avoid that a pipeline/job handler delete credentials, while we also start our loop to avoid double deletion.
    // Therefore we remember credentials that were selected for deletion, and delete them really only the second time we
    // encounter them. We loop every 5 minutes, if a job was in success/failed/canceled state  the first time, and 5 minutes
    // later it is still in the database, then clearly the original job-webhook-handler failed to delete it
    credentialSeen := make(map[Credential]int)
    for {
        if all_creds, err := td.db.GetAllCredentials(); err != nil {
            logging.Error(err, "Failed getting all credentials from database")
        } else {
            jobs_status := map[int]string{}
            for _, cred := range all_creds {
                if jobs_status[cred.JobID] == "" {
                    // cache job status
                    if job, _, err := glserver.Jobs.GetJob(cred.GlProjectID, cred.JobID); err != nil {
                        logging.Errorf(err, "Failed fetching job from gitlab with gitlab_project_id=%v and job_id=%v", cred.GlProjectID, cred.JobID)
                        credentialSeen[cred] += 1
                        if credentialSeen[cred] > 10 {
                            // force deletion after we could not get the job's status often enough (probably deleted on gitlab's side)
                            logging.Infof("Deleting credential %v for job_id=%v because we could not fetch the job status often enough", cred.Username, cred.JobID)
                            td.DeleteToken(cred)
                            delete(credentialSeen, cred)
                        }
                        // do nothing if we cannot get the job's current status
                        continue
                    } else {
                        jobs_status[cred.JobID] = job.Status
                    }
                }

                if jobs_status[cred.JobID] == GlStatusRunning {
                    // do nothing if the job is still running
                    continue
                }
                if _, ok := credentialSeen[cred]; ok {
                    logging.Infof("Deleting credential %v for job_id=%v", cred.Username, cred.JobID)
                    td.DeleteToken(cred)
                    delete(credentialSeen, cred)
                } else {
                    credentialSeen[cred] = 1
                }
            }
        }

        time.Sleep(300 * time.Second)
    }
}

