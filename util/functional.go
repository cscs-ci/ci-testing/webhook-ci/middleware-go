package util;

func SliceMap[T, U any](in []T, f func(T) U) []U {
    res := make([]U, 0, len(in))
    for _, e := range in {
        res = append(res, f(e))
    }
    return res
}

func SliceFilter[T any](in []T, f func(T) bool) []T {
    res := make([]T, 0)
    for _, e := range in {
        if f(e) {
            res = append(res, e)
        }
    }
    return res
}

func SliceContains[T comparable](in []T, cmp T) bool {
    for _, e := range in {
        if (e == cmp) {
            return true
        }
    }
    return false
}
