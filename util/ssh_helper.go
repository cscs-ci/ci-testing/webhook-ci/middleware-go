package util;

import (
    "crypto/ed25519"
    "encoding/pem"
    "golang.org/x/crypto/ssh"
    "os"

    "github.com/mikesmitty/edkey"
)

// returns the pubkey
func CreateSshKeys(path string) ([]byte, error) {
    // see https://stackoverflow.com/questions/71850135/generate-ed25519-key-pair-compatible-with-openssh
    pubKey, privKey, err := ed25519.GenerateKey(nil)
    if err != nil { return nil,err }
    publicKey, err := ssh.NewPublicKey(pubKey)
    if err != nil { return nil, err }

    pemKey := &pem.Block{
        Type:  "OPENSSH PRIVATE KEY",
        Bytes: edkey.MarshalED25519PrivateKey(privKey),
    }
    privateKeyBytes := pem.EncodeToMemory(pemKey)
    authorizedKeyBytes := ssh.MarshalAuthorizedKey(publicKey)

    err = os.WriteFile(path, privateKeyBytes, 0600)
    if err != nil { return nil, err }
    // nosemgrep: gosec.G302-1
    err = os.WriteFile(path+".pub", authorizedKeyBytes, 0644) //#nosec G306 -- file permissions are fine
    if err != nil { return nil, err }
    return authorizedKeyBytes, nil
}
