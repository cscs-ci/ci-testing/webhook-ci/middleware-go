package main

import (
    "flag"
    "time"

    "github.com/rs/zerolog"

    "cscs.ch/cicd-ext-mw/handler"
    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)



func main() {
    logging.SetLogLevels(zerolog.InfoLevel, zerolog.DebugLevel, zerolog.DebugLevel)

    var configpath string
    flag.StringVar(&configpath, "config", "config.yaml", "Path to config YAML file")
    flag.Parse()

    logging.Infof("Using configpath=%q", configpath)
    config := util.ReadConfig(configpath)

    util.SetupGitlab(config.Gitlab.Url, config.Gitlab.Token)
    glserver := util.GetGitlab()

    util.SetDBPath(config.GetDBPath())
    db := util.GetDb()
    defer db.Close()

    handler.GetSessionStore().Cleanup(time.Minute*5)

    tokenDeleter := util.NewTokenDeleter(glserver, db, config)
    go tokenDeleter.Watch() // run infinite loop in background and do allow further background tasks

    backgroundNotifier := util.NewBackgroundNotifier(glserver, db, config)
    go backgroundNotifier.Watch()

    if config.HeartbeatConfig.URL != "" && config.HeartbeatConfig.PeriodSec > 0 {
        go func() {
            for {
                if resp, err := util.DoJsonRequest("POST", config.HeartbeatConfig.URL, nil,nil); err != nil {
                    logging.Error(err, "Failed POST to heartbeat url.")
                } else {
                    if err := util.CheckResponse(resp); err != nil {
                        logging.Error(err, "Failed checking response to heartbeat url")
                    }
                }
                time.Sleep(time.Duration(config.HeartbeatConfig.PeriodSec) * time.Second)
            }
        }()

        }

    if config.EnableCron {
        cronScheduler := util.NewCronScheduler(glserver, db, config)
        go cronScheduler.ScheduleJobs()
    }

    runnersObserver := util.NewRunnersObserver(glserver, config.RunnerObserver.Output, config.RunnerObserver.Runners)
    runnersObserver.Observe() // run in foreground and block forever
}
